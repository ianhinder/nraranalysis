(* Copyright 2014 Ian Hinder

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

BeginPackage[
  "NRARAnalysis`Result`", 
  {"SimulationTools`",
   "SimulationTools`CoordinateTransformations`",
   "SimulationTools`DataTable`",
   "SimulationTools`DataRepresentations`",
   "SimulationTools`Memo`",
   "SimulationTools`NRDF`",
   "SimulationTools`Waveforms`",
   "NRARAnalysis`Extrapolation`",
   "NRARAnalysis`Configuration`",
   "NRARAnalysis`Utils`"}];

FinalStrain;
finalStrain;
finalPsi4;
ReferenceTimeFromFinalStrain;
FinalStrainEndTime;
waveformMaxTime;
WaveformEndTime;

$ExtrapolateBeforeStrain = False;

Begin["`Private`"];

FinalStrain[run_String] :=
  finalStrain[run, strainFrequency[run],
              If[HaveInfiniteRadiusWaveforms[run], {None,None}, extrapOrder[run]]];

DefineMemoFunction[finalStrain[run_String, om:(_?NumberQ), ord_List],
  Module[
    {nRadii},
    log["Computing strain"];
    If[HaveInfiniteRadiusWaveforms[run],
       StrainFromPsi4[ReadPsi4[run,2,2,Infinity], om],
       (* else *)
       Module[{om1 = If[om===Automatic, strainFrequency[run], om]},
       If[$ExtrapolateBeforeStrain,
       Psi4ToStrain[ReadRadiallyExtrapolatedPsi4[
         run, 2, 2,
         ord, Radii -> extrapRadiiForRun[run],
         RadialCoordinateTransformation -> RadialToTortoise,
         DiscretePhaseAlignmentTime -> UsefulWaveformTime[run]], om1],
       ReadRadiallyExtrapolatedStrain[
         run, 2, 2,
         om1, ord, Radii -> extrapRadiiForRun[run],
         RadialCoordinateTransformation -> RadialToTortoise,
         DiscretePhaseAlignmentTime -> UsefulWaveformTime[run]]]
             ]
]]];

finalStrain[run_String, om:(_?NumberQ)] :=
  finalStrain[run, om, If[HaveInfiniteRadiusWaveforms[run], {None,None}, extrapOrder[run]]];

DefineMemoFunction[finalPsi4[run_String],
  Module[
    {nRadii},
    log["Computing psi4"];
    If[HaveInfiniteRadiusWaveforms[run],
       ReadPsi4[run,2,2,Infinity], 
       (* else *)
       ReadRadiallyExtrapolatedPsi4[
         run, 2, 2, 
         extrapOrder[run], Radii -> extrapRadiiForRun[run], 
         RadialCoordinateTransformation -> RadialToTortoise,
         DiscretePhaseAlignmentTime -> UsefulWaveformTime[run]]]]];

DefineMemoFunction[ReferenceTimeFromFinalStrain[run_String],
  Module[
    {h, tMax, t, freqFn},
    h = FinalStrain[run];

    (* This message appears to be new in Mathematica 9 and probably
       occurs because the data is noisy.  It would be better to locate
       the maximum point and fit a low order polynomial in the
       neighbourhood and take the analytic maximum of the polynomial.
       This avoids coupling gradient-descent maximisation to noisy
       data, and reduces it to a straightforward least squares fit. *)
    tMax = Quiet[LocateMaximum[Abs[h]],FindMaximum::eit];
    freqFn = Interpolation[Abs[Frequency[h]]];
    t/.FindRoot[Evaluate[freqFn[t] == 0.2], {t, tMax, tMax - 100}]]];

FinalStrainEndTime[run_] :=
 WaveformEndTime[FinalStrain[run]];

waveformMaxTime[run_] := 
  LocateMaximumPoint[Abs@FinalStrain[run]];

WaveformEndTime[waveform_DataTable] :=
 Module[{f, tMax, t, tEnd},
  f = Interpolation[Abs@waveform];
  tMax = LocateMaximumPoint[Abs@waveform];
  tEnd = t /. Quiet[
    FindRoot[
     f[t] == 1.0*10^-5, {t, tMax, 
      tMax + 10}], {InterpolatingFunction::dmval, FindRoot::cvmit}];

  If[tEnd > DataTableRange[waveform][[2]] - 20, tEnd = DataTableRange[waveform][[2]]];
  If[tEnd < tMax, tEnd = DataTableRange[waveform][[2]]];
  tEnd];

End[];

EndPackage[];
