\documentclass[aps,prd,amsmath,nofootinbib,10pt,showpacs,preprintnumbers,notitlepage,twocolumn]{revtex4-1}
\usepackage{graphicx}
\usepackage{color}
\usepackage[retainorgcmds]{IEEEtrantools}
\usepackage{hyperref}
\usepackage{dcolumn}
\usepackage{multirow}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% New Commands
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\interfootnotelinepenalty=10000

\begin{document}

\raggedbottom
\renewcommand{\arraystretch}{1.2}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{Error analysis of NR data submitted to the NR-AR repository}

\author{Ian Hinder}
\email{ian.hinder@aei.mpg.de}
\affiliation{
Max-Planck-Institut f\"ur Gravitationsphysik,
Albert-Einstein-Institut, \\ 
Am M\"uhlenberg 1, D-14476 Golm, Germany}

\date{\today}

\begin{abstract}
This document contains a preliminary analysis of numerical error in
the data which has been submitted so far to the NR-AR data repository.  
\end{abstract}

\maketitle

\tableofcontents

%\clearpage

\section{Introduction}

This document describes the error analysis of the NR-AR data.  It is
split between the initial analysis of $\Psi_4$ which is closest to the
NR data, and an analysis of the errors in $h$ as will be used for AR.
This split is mostly historical and will be tidied up.

\section{$\Psi_4$ analysis}

Table \ref{tbl:summary} shows a summary of the error analysis.
Initially, we consider only the lowest extraction radius waveform, as
it is the least likely to be affected by low resolution in the coarse
grids of AMR simulations.  The {\em Expected order} column is the
expected order of convergence of the simulation as reported by each
group.  This is typically the order of spatial finite differencing, or
{\em exponential} for spectral codes.  The reference time,
$t_\mathrm{ref}$, is the coordinate time at which the GW frequency of
the highest resolution waveform is $0.2/M$.  The {\em Measured order}
is the measured order of convergence of
$\phi_\mathrm{gw}(t_\mathrm{ref})$; i.e. the gravitational wave
($\Psi_4^{2,2}$) phase at the reference time.  The {\em Monotonic}
column indicates whether the time at which the waveform reaches its
maximum amplitude is monotonic with resolution or not.  This is
expected for solutions within the convergent regime.  The {\em Error}
column gives the difference in radians between
$\phi_\mathrm{gw}(t_\mathrm{ref})$ at the highest resolution and its
Richardson extrapolant, assuming the {\em Expected order} of
convergence.  In cases where the measured order does not match the
expected order, this is merely an indication of the possible error in
the waveform, and should not be treated as anything more.
%% Each waveform has had a constant phase shift applied at
%% a fixed time after the junk radiation so that they agree at that
%% point.
Each waveform has had a time shift applied such the waveforms agree in
an interval a fixed time after the junk radiation in the sense of a
least-squares fit.  A phase alignment has also been performed at this
time.
%% 
%% Each waveform has had a constant phase shift applied at a fixed time
%% approximately 8 orbits before the peak of the waveform.

\section{$h$ analysis}

In the plots for each configuration, we include a plot of $h$ as well
as the errors in $h$ as a function of time.  We give the errors on
both a linear and a log scale.  The strain is computed as described in
Sec.~\ref{sec:straincomp} from each finite radius $\Psi_4$ waveform at
each numerical resolution.  The resulting waveform is extrapolated as
described in Sec.\ref{sec:extrap}.

\subsubsection{Extrapolation to infinite radius}
\label{sec:extrap}

The strain is extrapolated in amplitude and phase by first
time-shifting with the Schwarzschild tortoise coordinate to account
for propagation time between different radii (the mass used is the ADM
mass) and then fitting to a polynomial dependence on radius.  The
order, $p$, of extrapolation is generally 2, but in some cases it is
lower because only a small number of radii were provided.  The
extrapolation error is computed by subtracting the extrapolant with
order $p+1$.


\subsubsection{Conversion from $\Psi_4$ to $h$}
\label{sec:straincomp}

After discussions and tests, we concluded that the most useful and
accurate procedure to compute the strain, $h$, from $\Psi_4$ is as follows:

\begin{itemize}
\item We {\em want} to include the junk radiation in the process. This
  is extremely important if one wants to compute correctly the GW
  (energy and angular momentum) losses throughout the
  evolution. Having access to such information is very useful for
  comparison with analytical results that are different from the
  simple phasing.  For example, in arXiv:1110.2938 one needs to
  measure accurately the GWs losses (notably including the junk
  radiation) so to compute the relation between the total energy
  ${\cal E}$ of the binary system and its total angular momentum
  ${\cal J}$. Addressing with $\Delta {\cal J}$ and $\Delta {\cal E}$
  the energy and angular momentum GW losses, the total energy and
  angular momentum of the binary are given by
  \begin{eqnarray}
    {\cal E} &=& {\cal E}_0 - \Delta {\cal E}\\
    {\cal J} &=& {\cal J}_0 - \Delta {\cal J}
  \end{eqnarray}
  
  where ${\cal E}_0$ and ${\cal J}_0$ are the initial ADM energy and
  angular momentum of the system.  The gauge-invariant relation ${\cal
    E}({\cal J}$ extracted from numerical simulations can then be
  compared to analytical predictions. This allows then for a direct
  comparison between the numerically defined and analytically defined
  {\it dynamics}.
      
\item The need to properly include the junk radiation calls for a
  generalization of the frequency-domain integration procedure
  introduced by Reisswig \& Pollney, in that one needs to exclude an
  interval of frequencies $[-\omega_0,\omega_0]^{\ell m}$ around the
  origin. This frequency interval will depend on the multipole
  ($\ell,m$).

\item The procedure depends on the choice of the cutting frequency
  $\omega_0$. This frequency should be large enough to determine
  properly the integration constants (i.e., practically speaking,
  smoothing the spurious oscillations that otherwise appear in the
  modulus of $h$), but also small enough so to give a physically
  reliable representation of the junk radiation.  In practice, one
  choses $\omega_0^{22}$ to be about a $20\%$ smaller than the GW
  frequency of the beginning of the inspiral [Ian: we actually use 0.5
    times the initial frequency].  The corresponding cutting
  frequencies for the other multipoles is simply obtained from the
  relation

  \begin{equation}
    \omega_0^{\ell m} = \frac{\omega_0^{2 2}}{2} \times m
  \end{equation}

\item The dependence of the phasing was explored by varying $\omega_0$
  around this value by a factor of 1.5.  One estimates that the error
  on phasing due to the integration procedure is $\lesssim 0.01$
  radians.

\item After checks, it turned out that the best way to prepare a
  ``final'' strain waveform at infinite extraction radius is to do the
  following:
     
\begin{itemize}
\item compute the strain for each extraction radius, $h(r,t)$;
\item extrapolate the various $h(r,t)$ for $r\to\infty$;
\end{itemize}
   this procedure is more accurate and robust than extrapolating
   $\Psi_4$ first and then taking the double integration. [Ian: we
     should explain this more]

 \item One also applies the same integration technique to compute one
   time integral of $h$, so to get $\dot{h}$ at infinite extraction
   radius. Similarly, one first integrates and then extrapolates so to
   have $\dot{h}$ at infinite extraction radius. [Ian: we don't
     provide $\dot{h}$ currently, but this would be easy to do. I
     would prefer to numerically differentiate $h$ rather than doing
     the whole integration again.]
\end{itemize}

\vspace{12pt}

%\begin{widetext}

\begin{table*}
\input{report/summary}
\caption{Summary of the properties of each configuration submitted to
  the NRAR data repository.  Configurations with only a single
  resolution or with invalid metadata files are excluded.}
\label{tbl:summary}
\end{table*}
%\end{widetext}

\begin{verbatim}
\end{verbatim}

\clearpage

\section{Detailed Results}

\input{report/codes}

\end{document}
