(* Copyright 2014 Ian Hinder

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

BeginPackage["NRARAnalysis`ARCompare`", {"SimulationTools`", "SimulationTools`RunFiles`", "SimulationTools`DataTable`", "SimulationTools`DataRepresentations`", "SimulationTools`ColumnFile`", "SimulationTools`NRDF`", "SimulationTools`Plotting`", "SimulationTools`Waveforms`",
  "NRARAnalysis`AnalyseRepo`"}];

CompareARConfigs;
$ARCompareConfig;
CompareInspiralWaveforms;
CompareMergerWaveforms;
CompareInspiralErrors;
CompareMergerErrors;
CompareARConfigurations;

Begin["`Private`"];

convertToAbs[{A_DataTable, phi_DataTable, DA_DataTable, Dphi_DataTable, dA_DataTable, dphi_DataTable}] :=
  {A, phi, A DA, Dphi, A dA, dphi};

DataOfARConfig[configDir_String] :=
  Table[($ARCompareConfig=FileNameTake[dir, -2]) -> 
   convertToAbs@Table[MakeDataTable@
     ReadColumnFile[FileNameJoin[{dir, "hAmpPhase_l2_m2.asc"}]][[
      All, {1, i}]],
    {i, 2, 7}], {dir, 
   Select[FileNames["*/*", configDir], (DirectoryQ[#] && !StringMatchQ[#,"*.git*"]) &]}];

cols = {"A", "\[Phi]", "\[CapitalDelta]A", "\[CapitalDelta]\[Phi]", 
   "\[Delta]A", "\[Delta]\[Phi]"};

CompareARData[d1_DataTable, d2_DataTable] :=
 If[Length[d1] =!= Length[d2],
  "Different lengths (" <> ToString[Length[d1]] <> "," <> 
   ToString[Length[d2]] <> ")",
  Max[Abs[d1 - d2]]];

CompareARConfig[data_, configName_String] :=
 Module[{ds1, ds2},
  ds1 = configName /. data[[1]];
  ds2 = configName /. data[[2]];
  If[MatchQ[{ds1, ds2}, {{_DataTable ...}, {_DataTable ...}}],
   MapThread[CompareARData,
    {ds1, ds2}],
   If[StringQ[ds1] && ListQ[ds2],
    {"Only in 2", SpanFromLeft, SpanFromLeft, SpanFromLeft},
    If[ListQ[ds1] && StringQ[ds2],
     {"Only in 1", SpanFromLeft, SpanFromLeft, SpanFromLeft},
     {"Not datatables", SpanFromLeft, SpanFromLeft, SpanFromLeft}]]]]

CompareARConfigs[config1Dir_String, config2Dir_String] :=
  Module[{data1, data2, data, configNames},
    data1 = DataOfARConfig[config1Dir];
    data2 = DataOfARConfig[config2Dir];
    data = {data1, data2};
    configNames = Union[Map[First, data[[1]]], Map[First, data[[2]]]];
    Global`$data=data;
    Grid[Prepend[Table[Prepend[CompareARConfig[data, c], c], {c, configNames}], 
      Prepend[cols, ""]], Alignment -> Left, Frame -> True, 
      Dividers -> All, Spacings -> {1, 0.5}, ItemStyle -> (FontFamily -> "Times")]];

CompareInspiralWaveforms[runs_List] :=
 Module[{tJunk, dps, dAs, dpRefs, dARefs, configs, config, tMin, tMax},
  tJunk = UsefulWaveformTime[runs[[1]]];
  
  configs = FileNameJoin[FileNameTake[#, -2]] & /@ runs;
  If[! Equal @@ configs, 
   Error["Mismatched configurations: " <> 
     StringJoin[Riffle[runs, ", "]]]];
  config = configs[[1]];
  
  tMin = 0;
  tMax = tJunk + 1000;
  dps = DataTableInterval[#, {tMin, tMax}] & /@ 
    AlignPhases[ReadPhase /@ runs, 500];
  
  dAs = DataTableInterval[ReadAmplitude[#], {tMin, tMax}] & /@ runs;
  
  {Show[
    PresentationListLinePlot[dps, 
     PlotRange -> {{tMin, tMax}, {0, 1.5 Max[dps]}}, 
     AxesOrigin -> {0, 0}, PlotLegend -> {"Run 1", "Run 2"}, 
     PlotLabel -> config, ImageSize -> 400, 
     FrameLabel -> {"t/M", "\[Phi]"}]],
   
   Show[
    PresentationListLinePlot[dAs, 
     PlotRange -> {{tMin, tMax}, {0, 1.5 Max[dAs]}}, 
     AxesOrigin -> {0, 0}, PlotLegend -> {"Run 1", "Run 2"}, 
     PlotLabel -> config, ImageSize -> 400, 
     FrameLabel -> {"t/M", "A"}]],
   
   Show[
    PresentationListLinePlot[Re /@ (dAs Map[Exp[I #] &, dps]), 
     PlotRange -> {{tMin, tMax}, {-1, 1} 1.5 Max[dAs]}, 
     AxesOrigin -> {0, 0}, PlotLegend -> {"Run 1", "Run 2"}, 
     PlotLabel -> config, ImageSize -> 400, 
     FrameLabel -> {"t/M", "A"}]]}
  ]

CompareMergerWaveforms[runs_List] :=
 Module[{tRef, dps, dAs, dpRefs, dARefs, configs, config, tMin, 
   tMax},
  tRef = ErrorReferenceTime[runs[[1]]];
  
  configs = FileNameJoin[FileNameTake[#, -2]] & /@ runs;
  If[! Equal @@ configs, 
   Error["Mismatched configurations: " <> 
     StringJoin[Riffle[runs, ", "]]]];
  config = configs[[1]];
  
  tMin = tRef - 200;
  tMax = tRef + 20;
  
  dps = DataTableInterval[#, {tMin, tMax}] & /@ 
    AlignPhases[ReadPhase /@ runs, 500];
  dpRefs = 
   Interpolation[DataTableInterval[#, {tRef - 20, tRef + 20}]][
      tRef] & /@ dps;
  
  dAs = DataTableInterval[ReadAmplitude[#], {tMin, tMax}] & /@ runs;
  dARefs = 
   Interpolation[DataTableInterval[#, {tRef - 20, tRef + 20}]][
      tRef] & /@ dAs;
  
  {Show[
    PresentationListLinePlot[dps, 
     PlotRange -> {{tMin, tMax}, {0, 1.5 Max[dpRefs]}}, 
     AxesOrigin -> {0, 0}, PlotLegend -> {"Run 1", "Run 2"}, 
     PlotLabel -> config, ImageSize -> 400, 
     FrameLabel -> {"t/M", "\[Phi]"}], 
    Graphics[{Dotted, Line[{{tRef, -10}, {tRef, 10}}], 
      Table[Line[{{0, dpRef}, {20000, dpRef}}], {dpRef, dpRefs}]}]],
   
   Show[
    PresentationListLinePlot[dAs, 
     PlotRange -> {{tMin, tMax}, {0, 1.5 Max[dARefs]}}, 
     AxesOrigin -> {0, 0}, PlotLegend -> {"Run 1", "Run 2"}, 
     PlotLabel -> config, ImageSize -> 400, 
     FrameLabel -> {"t/M", "A"}], 
    Graphics[{Dotted, Line[{{tRef, -10}, {tRef, 10}}]}]]}
  
  ]


CompareMergerErrors[runs_List] :=
 Module[{tRef, dps, dAs, dpRefs, dARefs, configs, config, tMin, 
   tMax},
  tRef = ErrorReferenceTime[runs[[1]]];
  
  configs = FileNameJoin[FileNameTake[#, -2]] & /@ runs;
  If[! Equal @@ configs, 
   Error["Mismatched configurations: " <> 
     StringJoin[Riffle[runs, ", "]]]];
  config = configs[[1]];
  
  tMin = tRef - 200;
  tMax = tRef + 20;
  
  dps = DataTableInterval[
      ReadPhaseError[#, AlignedErrors -> False], {tMin, tMax}] & /@ 
    runs;
  dpRefs = 
   Interpolation[DataTableInterval[#, {tRef - 20, tRef + 20}]][
      tRef] & /@ dps;
  
  dAs = DataTableInterval[
      ReadAmplitudeError[#, AlignedErrors -> False], {tMin, tMax}] & /@
     runs;
  dARefs = 
   Interpolation[DataTableInterval[#, {tRef - 20, tRef + 20}]][
      tRef] & /@ dAs;
  
  {Show[
    PresentationListLinePlot[dps, 
     PlotRange -> {{tMin, tMax}, {0, 1.5 Max[dpRefs]}}, 
     AxesOrigin -> {0, 0}, PlotLegend -> {"Run 1", "Run 2"}, 
     PlotLabel -> config, ImageSize -> 400, 
     FrameLabel -> {"t/M", "\[CapitalDelta]\[Phi]"}], 
    Graphics[{Dotted, Line[{{tRef, -10}, {tRef, 10}}], 
      Table[Line[{{0, dpRef}, {20000, dpRef}}], {dpRef, dpRefs}]}]],
   
   Show[
    PresentationListLinePlot[dAs, 
     PlotRange -> {{tMin, tMax}, {0, 1.5 Max[dARefs]}}, 
     AxesOrigin -> {0, 0}, PlotLegend -> {"Run 1", "Run 2"}, 
     PlotLabel -> config, ImageSize -> 400, 
     FrameLabel -> {"t/M", "\[CapitalDelta]A/A"}], 
    Graphics[{Dotted, Line[{{tRef, -10}, {tRef, 10}}]}]]}
  
  ]

CompareInspiralErrors[runs_List] :=
 Module[{tRef, dps, dAs, dpRefs, dARefs, configs, config, tMin, 
   tMax},
  tRef = ErrorReferenceTime[runs[[1]]];
  
  configs = FileNameJoin[FileNameTake[#, -2]] & /@ runs;
  If[! Equal @@ configs, 
   Error["Mismatched configurations: " <> 
     StringJoin[Riffle[runs, ", "]]]];
  config = configs[[1]];
  
  tMin = 0;
  tMax = tRef - 20;
  
  dps = DataTableInterval[
      ReadPhaseError[#, AlignedErrors -> False], {tMin, tMax}] & /@ 
    runs;
  (*dpRefs=Interpolation[DataTableInterval[#,{tRef-20,tRef+20}]][
  tRef]&/@dps;
  *)
  dAs = 
   DataTableInterval[
      ReadAmplitudeError[#, AlignedErrors -> False], {tMin, tMax}] & /@
     runs;
  (*dARefs=Interpolation[DataTableInterval[#,{tRef-20,tRef+20}]][
  tRef]&/@dAs;
  *)
  {Show[
    PresentationListLinePlot[dps, 
     PlotRange -> {{tMin, tMax}, {0, 0.1}}, AxesOrigin -> {0, 0}, 
     PlotLegend -> {"Run 1", "Run 2"}, PlotLabel -> config, 
     ImageSize -> 400, 
     FrameLabel -> {"t/M", "\[CapitalDelta]\[Phi]"}]],
   
   Show[
    PresentationListLinePlot[dAs, 
     PlotRange -> {{tMin, tMax}, {0, 0.1}}, AxesOrigin -> {0, 0}, 
     PlotLegend -> {"Run 1", "Run 2"}, PlotLabel -> config, 
     ImageSize -> 400, FrameLabel -> {"t/M", "\[CapitalDelta]A/A"}]]}
  
  ]

(**************************)
(* New comparison code *)
(**************************)

CompareData[ds:{d1_DataTable, d2_DataTable}] :=
  {"MaxDelta" ->
    If[SameGridQ@@ds,
      Max[Abs[d1 - d2]],
      Max[Abs[Quiet[WithResampling[d1-d2],InterpolatingFunction::dmval]]]],
    "Max" -> Max[Max[d1],Max[d2]],
    "Grids" -> If[SameGridQ@@ds, Equal, Map[{CoordinateRange[#], Length[#]} &, ds]],
    "Plots" -> CompareDataPlot[ds]};

CompareDataPlot[ds:{d1_DataTable, d2_DataTable}] :=
  Module[{},
    {PresentationListLinePlot[ds,PlotRange->All], PresentationListLinePlot[Quiet[WithResampling[Subtract@@ds],InterpolatingFunction::dmval],PlotRange->All]}];

CompareARConfigurations[configs:{config1_String, config2_String}] :=
  Module[{},
    Block[{$Memoisation = True},
    As = ReadAmplitude /@ configs;
    phis = ReadPhase /@ configs;
    AErrs = As ReadAmplitudeError /@ configs;
    phiErrs = ReadPhaseError /@ configs;
    AAlErrs = As (ReadAmplitudeError[#, AlignedErrors -> True] & /@ configs);
    phiAlErrs = ReadPhaseError[#, AlignedErrors -> True] & /@ configs];

    {"Amplitude" -> CompareData[As],
      "Phase" -> CompareData[phis],
      "AmplitudeError" -> CompareData[AErrs],
      "PhaseError" -> CompareData[phiErrs],
      "AlignedAmplitudeError" -> CompareData[AAlErrs],
      "AlignedPhaseError" -> CompareData[phiAlErrs]}];

End[];

EndPackage[];
