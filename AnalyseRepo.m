(* Copyright 2014 Ian Hinder

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)


BeginPackage[
  "NRARAnalysis`AnalyseRepo`",
  {"SimulationTools`Waveforms`","SimulationTools`DataTable`", "SimulationTools`DataRepresentations`", "SimulationTools`NR`", "SimulationTools`Plotting`", "SimulationTools`Convergence`",
   "SimulationTools`Memo`", "SimulationTools`NRDF`", "SimulationTools`Error`", "SimulationTools`NRExport`", "SimulationTools`RunFiles`", "SimulationTools`ColumnFile`", "SimulationTools`MessageCatcher`", "SimulationTools`ReadHDF5`", "NRARAnalysis`LaTeX`",
   "NRARAnalysis`Configuration`",
   "NRARAnalysis`Extrapolation`",
   "NRARAnalysis`Export`",
   "NRARAnalysis`Result`",
   "NRARAnalysis`Utils`"}];

(*********************************************************************************************
  Definitions
 *********************************************************************************************)

ARReport;
AlignedWaveformsCCC;
AlignmentWindowCCC;
ClearLog;
ConfigSummary;
NRARConfigurations;
PlotWaveforms;
ReportOfConfiguration;
StrainAmplitudeErrors;
StrainPhaseErrors;
StrainErrors;
ampPhaseErrorsOfWaveforms;
log;
finalStrainExtrapError;
finalStrainTruncError;
finalStrainFFIError;
RunsForTruncationError;
HaveRunsForTruncationError;

ReadAmplitudeError
ReadPhaseError
ReadAmplitude;
ReadPhase;
ErrorReferenceTime
AlignmentWindow

(* These are for the AR data - rename to clarify*)
TruncationErrorAlignmentShifts
ExtrapolationErrorAlignmentShifts
FFIErrorAlignmentShifts;

(* These are for the NR data - rename to clarify *)
truncationErrorAlignmentShifts
extrapolationErrorAlignmentShifts
ffiErrorAlignmentShifts;

FinalStrainRun;

InitialiseParallelNRARAnalysis;

Begin["`Private`"];

sub = SimulationTools`DataRepresentations`Sub;

(**************************)
(* Waveforms *)
(**************************)

HaveRunsForTruncationError[config_String] :=
  Length[RunsInConfiguration[config]] >= 2;

RunsForTruncationError[config_String] :=
  If[HaveRunsForTruncationError[config],
     Take[RunsInConfiguration[config],-2],
     Error["Configuration "<>config<>" has insufficient runs to compute the truncation error"]];

FinalStrainRun[config_String] :=
  HighResolutionRun[config];

(**************************)
(* Alignment *)
(**************************)

windows = {{"BAM/EOB_q1._0.5_0.1_D30D12.5162", 500., 
  1190.}, {"BAM/EP_um2D10_d6_sz", 300., 
  800.}, {"BAM-FAU/o.022e4q1S0.6_60_0_S0.6_120_90", 500., 
  1240.}, {"BAM-FAU/om0.025etmq3S0.4_0_0_S0.6_0_0", 500., 
  1200.}, {"GATech/Q1_a0.60_a0.60", 500., 
  1200.}, {"GATech/Q2_a-0.15_0.60", 500., 
  1200.}, {"GATech/Q2_a0.30_0.00", 500., 
  1200.}, {"GATech/Q2_a0.60_0.60", 500., 1200.}, {"LazEv-RIT/q10", 
  300., 800.}, {"Lean-Contrib/dq4", 500., 
  1200.}, {"Llama-AEI/Q1S3", 500., 1200.}, {"Llama-AEI/Q1S6", 480., 
  1130.}, {"Llama-AEI/Q1S-3", 480., 1220.}, {"Llama-AEI/Q3T60S6", 0., 
  0.}, {"Llama-Palma-Caltech/q1a8a4", 630., 
  1600.}, {"Llama-Palma-Caltech/q1a8a-4", 630., 
  1650.}, {"UIUC/q1_spin_0_and_spin_0.3_aligned", 500., 
  1200.}, {"SpEC-CCC/SpEC-Aligned44", 800., 
  2150.}, {"SpEC-CCC/SpEC-Anti44", 610., 
  1850.}, {"SpEC-CCC/SpEC-Case01", 950., 
  2500.}, {"SpEC-CCC/SpEC-Case02", 860., 
  2650.}, {"SpEC-CCC/SpEC-Case03", 830., 
  2380.}, {"SpEC-CCC/SpEC-Case06", 900., 
  2750.}, {"SpEC-CCC/SpEC-Case07a", 1020., 
  2110.}, {"SpEC-CCC/SpEC-Case10", 1060., 
  2110.}, {"SpEC-CCC/SpEC-Case32", 650., 2000.}};

DefineMemoFunction[
  AlignmentWindowCCC[config_String],
  Module[
    {runs, strains, phases, matches, M},
    runs = RunsForTruncationError[config];
    matches = Cases[windows, {config, t1_, t2_} -> {t1,t2}];
    If[matches === {}, 
       (* Print["Using automatic alignment window for ", config]; *)
       strains = Map[FinalStrain, runs];
       phases = AlignPhases[Map[Phase, strains],UsefulWaveformTime[runs[[1]]]];
       AlignmentWindowCCC[runs, phases],
    
       (* Print["Using prescribed alignment window"]; *)
       M = ReadTotalMass[runs[[-1]]];
       M matches[[1]]]]];

AlignmentWindowCCC[runs_List, phases_List] :=
  Module[
    {freq, t1, t2, f1, f2, t},

    t1 = UsefulWaveformTime[runs[[1]]];
    freq = Abs@DataTableInterval[NDerivative[phases[[-1]]],{t1,All}];
    f1 = First[DepVar[freq]];
    t2 = Select[ToList[freq], #[[2]] > 1.15 f1 &,1][[1,1]];

    If[Abs[t2 - t1] < 100,
       Error["AlignedWaveformsCCC:alignmentWindow: Time range is too short: "<>ToString[{t1,t2}]<> " (the frequency at early times might be too oscillatory)"]];
    If[t1 > t2,
       Error["AlignedWaveformsCCC:alignmentWindow: Time range is bad: "<>ToString[{t1,t2}]]];
    If[t2 > DataTableRange[phases[[-1]]][[2]],
       Error["AlignedWaveformsCCC:alignmentWindow: Time range "<>ToString[{t1,t2}]<>
             " is outside waveform range: "<>ToString[DataTableRange[phases[[-1]]]]]];

    {t1, t2}];

AlignedWaveformsCCC[runs : {_String, _String},
                    waveforms: {_DataTable, _DataTable}] :=
  AlignedWaveformsCCC[waveforms, AlignmentShiftsCCC[runs,waveforms]];

AlignedWaveformsCCC[{waveform1_DataTable, waveform2_DataTable},
                    {dt_?NumberQ, dp_?NumberQ}] :=
  {waveform1, Exp[I dp] ShiftDataTable[dt, waveform2]};

AlignmentShiftsCCC[runs:{_String, _String}, waveforms:{_,_}] :=
  (* Compute the time and phase shift that need to be added to the
     second waveform such that the two waveforms are aligned in the
     alignment window *)
  Module[
    {phases, int, deltaphi, xi, alignmentShifts, dt, dtMin, dpMin, t2phi2, tRange},

    If[Length[runs] =!= 2,
       Error["AlignmentShiftsCCC: Expecting exactly two runs"]];
    If[Length[waveforms] =!= 2,
       Error["AlignmentShiftsCCC: Expecting exactly two phases"]];

    phases = AlignPhases[Map[Phase, waveforms],UsefulWaveformTime[runs[[1]]]];

    int[d : DataTable[l_, attrs___]] :=
    Module[
      {tList, dt, f},
      tList = IndVar[d];
      dt = Drop[RotateLeft[tList, 1] - tList, -1];
      f = Drop[DepVar[d], -1];
      Total[f dt]];
    
    deltaphi[{phi1_, phi2_}, dt_, tRange : {t1_, t2_}] :=
    int[DataTableInterval[phi1, tRange]~SimulationTools`DataRepresentations`Sub~
        ShiftDataTable[dt, DataTableInterval[phi2, tRange - dt]]]/(t2 - t1);

    xi[phis : {phi1_, phi2_}, dt_?NumberQ, tRange : {t1_, t2_}] :=
      int[((DataTableInterval[phi1, tRange]~SimulationTools`DataRepresentations`Sub~
        ShiftDataTable[dt, DataTableInterval[phi2, tRange - dt]]) - 
        deltaphi[phis, dt, tRange])^2];

    tRange = AlignmentWindowCCC[ConfigurationOfRun[runs[[1]]]];

    t2phi2 = DataTableRange[phases[[2]]][[2]];
    dtMin = dt /. Quiet[FindMinimum[xi[phases, dt, tRange], {dt, 0, tRange[[2]] - t2phi2, tRange[[1]]}],
                        {FindMinimum::lstol,FindMinimum::sdprec}][[2]];
    dpMin = deltaphi[phases, dtMin, tRange];

    {dtMin, dpMin}];

alignmentShiftFunction[None] = {0.,0.} &;
alignmentShiftFunction["CCC"] = AlignmentShiftsCCC;

(**************************)
(* Truncation error *)
(**************************)

$AlignmentMethod = "CCC";
(* $AlignmentMethod = None; *)

truncationErrorStrains[config_String] :=
  Map[FinalStrain,
      RunsForTruncationError[config]];

DefineMemoFunction[
  truncationErrorAlignmentShifts[config_String,align_],
  alignmentShiftFunction[If[align,$AlignmentMethod,None]][RunsForTruncationError[config],
                                           truncationErrorStrains[config]]];

finalStrainTruncError[config_String, align_] :=
  Module[
    {runs, runs1, strains, shifts, strains2, phases1, phases2, phaseErr,
     absAmpErr, relAmpErr},
    log["finalStrainTruncError"];
    runs = RunsForTruncationError[config];
    run1 = runs[[1]];
    strains = truncationErrorStrains[config];
    shifts = truncationErrorAlignmentShifts[config,align];
    strains2 = AlignedWaveformsCCC[strains, shifts];

    phaseErr = RichardsonError[AlignPhases[Phase/@strains2,UsefulWaveformTime[run1]],
                               1/(ReadResolution/@runs), ExpectedConvergenceOrder[run1]];

    absAmpErr = RichardsonError[Abs/@strains2,
                                1/(ReadResolution/@runs), ExpectedConvergenceOrder[run1]];

    relAmpErr = 100. * DivideDataTables[absAmpErr,Abs[strains2[[2]]]];
    {relAmpErr,phaseErr}];

(**************************)
(* Finite radius error *)
(**************************)

Options[finalStrainExtrapError] = Options[finalStrainTruncError];
finalStrainExtrapError[run_String, align_, OptionsPattern[]] :=
  Module[
    {p, pMax, nMax, strains1, strains, shifts, shift, phases, amps, phi, a, min, eAmp, eRelAmp, ePhase},
    log["finalStrainExtrapError"];

    If[HaveInfiniteRadiusWaveforms[run],
       Module[{zero = MapData[0. &, finalStrain[run, strainFrequency[run], {None,None}]]},
       Return[{zero,zero}]]];

    p = {pAmp, pPhase} = extrapOrder[run];

    pMax = Max[p];
    nMax = If[pMax == 0, 1, pMax + 2];

    (* Unaligned extrapolants *)
    strains1 = Table[finalStrain[run, strainFrequency[run], {n,n}], {n, 0, nMax}];

    strains = 
    If[align,
       (* Align each phase with the unextrapolated one *)
       shifts = Table[AlignmentShiftsCCC[{run,run}, {strains1[[1]], h}], {h, strains1[[2;;nMax+1]]}];
       (* shifts == {{dt2, dp2}, {dt3, dp3}, ...} *)
       shift[d_DataTable, {dt_?NumberQ, dp_?NumberQ}] :=
         Exp[I dp] ShiftDataTable[dt, d];
       (* Aligned extrapolants *)
       Flatten[{strains1[[1]], MapThread[shift, {strains1[[2;;nMax+1]], shifts}]}],

       (* else *)

       strains1];

    Module[{ranges, t1, t2, template},
    ranges = DataTableRange /@ strains;

    t1 = Max[ranges[[All,1]]];
    t2 = Min[ranges[[All,2]]];

    template = DataTableInterval[strains[[1]], {t1,t2}];

    strains = Map[ResampleDataTable[#,template, Intersect -> False] &, strains]];

    If[!Equal@@(DataTableRange/@strains),
       Print["Error!"];
       Error["finalStrainExtrapError: DataTable ranges are not equal"]];

    phases = AlignPhases[Phase/@strains,500];

    amps = Abs/@strains;

    Do[phi[n] = phases[[n+1]], {n,0,nMax}];
    Do[a[n] = amps[[n+1]], {n,0,nMax}];

    min[d1_DataTable, d2_DataTable] := MapThreadData[Min, {d1,d2}];

    (* Amplitude *)
    eAmp = Switch[pAmp,
                  2, min[Abs[a[2] - a[3]] + Abs[a[3]-a[4]], Abs[a[2] - a[0]]],
                  1, min[Abs[a[1] - a[2]] + Abs[a[2]-a[3]], Abs[a[1] - a[0]]],
                  0, Abs[a[0] - a[1]],
                  _, Error["Unsupported amplitude extrapolation order"]];
    eRelAmp = 100. eAmp/a[pAmp];

    (* Phase *)
    ePhase = Switch[pPhase,
                    2, min[Abs[phi[2] - phi[3]] + Abs[phi[3]-phi[4]], Abs[phi[2] - phi[0]]],
                    1, min[Abs[phi[1] - phi[2]] + Abs[phi[2]-phi[3]], Abs[phi[1] - phi[0]]],
                    0, Abs[phi[0] - phi[1]],
                    _, Error["Unsupported phase extrapolation order"]];

    {eRelAmp, ePhase}];

(**************************)
(* Strain conversion error *)
(**************************)

DefineMemoFunction[ffiErrorStrains[run_String],
  Module[{h,psi4,psi4p},
    h = FinalStrain[run];
    psi4 = finalPsi4[run];
    psi4p = NDerivative[NDerivative[h]];
    IntersectDataTables[{psi4,psi4p}]]];

  (* Map[finalStrain[run,#,Low] &, {strainFrequency[run], strainFrequency[run]*1.5}]; *)

DefineMemoFunction[
  ffiErrorAlignmentShifts[run_String,align_],
  alignmentShiftFunction[If[align,$AlignmentMethod,None]][{run,run}, ffiErrorStrains[run]]];

Options[finalStrainFFIError] = Options[finalStrainTruncError];
finalStrainFFIError[run_, align_, OptionsPattern[]] :=
  Module[
    {strains, shifts},
    log["finalStrainFFIError"];
    strains = ffiErrorStrains[run];
    shifts = ffiErrorAlignmentShifts[run,align];
    strains = AlignedWaveformsCCC[strains, shifts];
    ampPhaseErrorsOfWaveforms[run, strains]];

(**************************)
(* Errors *)
(**************************)

ampPhaseErrorsOfWaveforms[run_, waveforms_List] :=
  Module[
    {phaseErr,absAmpErr,relAmpErr},
    phaseErr = SimulationTools`DataRepresentations`Sub @@ AlignPhases[Phase /@ waveforms, UsefulWaveformTime[run]];
    absAmpErr = SimulationTools`DataRepresentations`Sub @@ (Abs /@ waveforms);
    relAmpErr = 100 DivideDataTables[absAmpErr,Abs[waveforms[[2]]]];
    {relAmpErr,phaseErr}];

StrainPhaseErrors[runs_List] := StrainErrors[runs, True][[2]];

StrainAmplitudeErrors[runs_List] := StrainErrors[runs, True][[1]];

DefineMemoFunction[StrainErrors[runs_List, align_],
  Module[
    {trunc, extrap, ffi, datas, quad, config, run},
    log["Computing strain error"];

    config = ConfigurationOfRun[runs[[1]]];
    run = FinalStrainRun[config];
    extrap = finalStrainExtrapError[run,align];

    res[d_DataTable,{i_}] := ResampleDataTable[d,extrap[[i]]];

    ffi = MapIndexed[res, finalStrainFFIError[run,align]];

    If[Length[runs] > 1,
       trunc = MapIndexed[res, finalStrainTruncError[config,align]],
       trunc = MapData[0. &, extrap]];

    datas = Map[IntersectDataTables, Transpose[{trunc,extrap,ffi}]];

    quad = Map[Function[errs, Sqrt[Plus@@Map[#^2 &, errs]]], datas];
    datas = MapThread[Append, {datas, quad}]]];




export[args___] := UsingFrontEnd[Export[args]];

(**************************)
(* Plotting *)
(**************************)

NRARPlotOptions = {LabelStyle -> 10, ImageSize -> 800, 
   ImageMargins -> 0};

VerticalLine[t_, {min_, max_}] := {Dotted, Line[{{t, min}, {t, max}}]};
HorizontalLine[y_, {xmin_, xmax_}] := {Dotted, Line[{{xmin, y}, {xmax, y}}]};

enlargeRange[{min_,max_}, f_] :=
  Module[{d},
         d = f(max-min);
         {If[min < 0, min - d, min], max + d}];

errorTrunc[run_, d_DataTable] :=
  DataTableInterval[d, {0, waveformMaxTime[run]+100}];

Options[PlotWaveforms] = Options[ListLinePlotWithLegend];
PlotWaveforms[run_String, datasp_List, fLabel_String, title_String, opts:OptionsPattern[]] :=
 Module[{om, datas, fRange, fRanges, tRanges,
   tRef = ReferenceTimeFromFinalStrain[run], 
   afterJunk = UsefulWaveformTime[run],
   beforeMerger, tEnd = FinalStrainEndTime[run], 
   tMax = LocateMaximumPoint[Abs@FinalStrain[run]],
   M = ReadTotalMass[run], refLine, maxLine, junkLine, window, windowLines, tStart},
  log["Plotting " <> ToString[title]];

  window = AlignmentWindowCCC[ConfigurationOfRun[run]];

  datas = (DataTableInterval[
    ResampleDataTable[#, 1.0, 1], {All, tEnd}] &) /@ datasp;

  tStart = Min[(DataTableRange/@datas)[[All,1]]];

  If[Or@@Map[Length[#] === 0 &, data] === 0, Error["Waveform to plot has zero length"]];

  fRanges = (enlargeRange[{Min[DataTableInterval[#,{afterJunk,tMax}]], 
            Max[DataTableInterval[#,{afterJunk,tMax}]]}, 0.1]) & /@ datas;

  fRange = {Min[fRanges[[All,1]]], Max[fRanges[[All,2]]]};

  beforeMerger = tRef - 100;
  tRanges = {{tStart, afterJunk/M + 100}, {afterJunk/M + 100, 
     beforeMerger/M}, {beforeMerger/M, tEnd/M}};

  refLine =
        {VerticalLine[tRef/M, fRange], 
         Text["M \[Omega] = 0.2",
              Offset[{-10,-10}, {tRef/M, fRange[[2]]}], {1, 1}]};

  maxLine = {VerticalLine[tMax/M, fRange],
             Text["max(A)", Offset[{10,-10}, {tMax/M, fRange[[2]]}], {-1, 1}]};

  junkLine = {VerticalLine[afterJunk/M, fRange],
             Text["Junk radiation", Offset[{-10,-10}, {afterJunk/M,fRange[[2]]}], {1, 1}]};

  windowLines = {VerticalLine[window[[2]]/M, fRange]};

  GraphicsRow[
   MapIndexed[
    PresentationListLinePlot[ScaledCoordinate[#,1/M]& /@ datas,
      PlotStyle -> OptionValue[PlotStyle],
      PlotRange -> {#1, fRange}, 
      AxesOrigin -> {0, 0},
      LegendPosition -> {Left, Top}, 
      FrameLabel -> {If[#2 === {2}, 
         "(t-\!\(\*SuperscriptBox[\(r\), \(*\)]\))/M", None], 
        If[#2 === {1}, fLabel, None]},
      ImagePadding -> {{If[#2 === {1}, 50, 12], 12}, {50, 1}}, 
      AspectRatio -> Full,
      Axes -> {True, False},
      FrameTicks -> {{If[#2 === {1}, Automatic, None], None}, {Automatic, None}},
      Epilog -> 
       Switch[#2,
             {1}, {junkLine},
             {3}, {refLine, maxLine, windowLines},
             {2}, {windowLines,
         Inset[
           If[OptionValue[PlotLegend] =!= {},
              MakePlotLegend[OptionValue[PlotLegend], OptionValue[PlotStyle],12,
                          FilterRules[{opts}, Options[MakePlotLegend]]], ""],
           Offset[{10,-10}, Scaled[{0,1}]], {Left,Top}]}, _, {}]] &, tRanges], Spacings -> 0, 
   Sequence @@ NRARPlotOptions, PlotLabel -> Style[title, 12]]];

NRARPlotColors = {Darker@Red, Darker@Green, Blue, Black};
NRARErrorPlotStyles = Join[NRARPlotColors, Map[Directive[Dashed, #] &, NRARPlotColors]];

(**************************)
(* Report *)
(**************************)

ReportOfConfiguration[config_String] :=
  Block[
    {$CurrentConfiguration = config},
    AbsoluteTiming[
      CheckAbort[
        LaTeXSection[
          config, config,
          Module[
            {M},
            log["Making report of configuration ", config];                                            
            Print["Making report of configuration ", config];                                            
            ClearAllMemos[];
            LaTeXCatch[
              If[ReadPsi4Radii[HighResolutionRun[config]] === {},
                 "No Psi4 data present for this configuration.",
                 (* else *)
                 {M = ReadTotalMass@HighResolutionRun@config;
                  LaTeXSection[
                    "strain", "Strain",
                    LaTeXCatch@Module[
                      {run = HighResolutionRun[config]},
                      GraphicsFile[
                        "strain.pdf",
                        GraphicsColumn[
                          {PlotWaveforms[run, {Re@FinalStrain[run]/M}, "Re[h]", "Real part of the strain waveform"],
                           PlotWaveforms[run, {Abs@FinalStrain[run]/M}, "A", "Amplitude of the strain waveform"],
                           PlotWaveforms[run, {Phase@FinalStrain[run]}, "\[Phi]", "Phase of the strain waveform"],
                           PlotWaveforms[run, {M Frequency@FinalStrain[run]}, "M \[Omega]", 
                                        "Frequency of the strain waveform"]}, Spacings -> 0, 
                          ImageSize -> 800, AspectRatio -> 1]]]],
                  LaTeXSection[
                    "strain-phase-errors", "Strain Phase Errors",
                    LaTeXCatch@Module[
                      {run = HighResolutionRun[config],
                       trunc,extrap,ffi,total, label = "\[CapitalDelta]\[Phi]",
                       truncU, extrapU, ffiU, totalU},
                      {trunc,extrap,ffi,total} = StrainErrors[RunsInConfiguration[config], True][[2]];
                      {truncU,extrapU,ffiU,totalU} = StrainErrors[RunsInConfiguration[config], False][[2]];
                      GraphicsFile["strain-phase-errors.pdf",
                                   PlotWaveforms[run, Abs@errorTrunc[run, #] & /@ {truncU, extrapU, ffiU, totalU, trunc, extrap, ffi, total},
                                                 label, "Phase errors", 
                                                 PlotLegend -> {"Truncation", "Radius", "Strain conversion", "Combined", "Truncation (aligned)",
                                                                "Radius (aligned)", "Strain conversion (aligned)", "Combined (aligned)"}, 
                                                 PlotStyle -> NRARErrorPlotStyles, ImageSize -> 800, AspectRatio -> 1]]]],
                  
                  LaTeXSection[
                    "strain-amplitude-errors", "Strain Amplitude Errors",
                    LaTeXCatch@Module[
                      {run = HighResolutionRun[config],
                       trunc, extrap, ffi, total, label = "\[CapitalDelta]A/A",
                       truncU, extrapU, ffiU, totalU},
                      {trunc,extrap,ffi,total} = 0.01 StrainErrors[RunsInConfiguration[config], True][[1]];
                      {truncU,extrapU,ffiU,totalU} = 0.01 StrainErrors[RunsInConfiguration[config], False][[1]];
                      GraphicsFile["strain-amp-errors.pdf",
                                   PlotWaveforms[run, Abs@errorTrunc[run, #] & /@ ({truncU, extrapU, ffiU, totalU, trunc, extrap, ffi, total}/M),
                                                 label, "Amplitude errors", 
                                                 PlotLegend -> {"Truncation", "Radius", "Strain conversion", "Combined", "Truncation (aligned)",
                                                                "Radius (aligned)", "Strain conversion (aligned)", "Combined (aligned)"}, 
                                                 PlotStyle -> NRARErrorPlotStyles, ImageSize -> 800, AspectRatio -> 1]]]], 
                  LaTeXSection[
                    "export", "Export", LaTeXCatch[ExportNRConfigurationAsAR[config, "export"]; "Success"]],
                  LaTeXSection[
                    "summary", "Summary", LaTeXCatch[ConfigSummary[config]]]}]]]], $Aborted]]];

ConfigSummary[config_String] :=
  Module[{run = HighResolutionRun[config], M},
  M = ReadTotalMass[run];
  Grid[{{"Name", config},
        {"Alignment window", If[HaveRunsForTruncationError[config],
                                ExportList[Round/@(AlignmentWindowCCC[config]/M)],
                                "-"]},
        {"Radii in extrapolation", If[!HaveInfiniteRadiusWaveforms[run],
                                     ExportList[Round/@(extrapRadiiForRun[run]/M)],
                                     "-"]},
        {"Radial extrapolation order", If[!HaveInfiniteRadiusWaveforms[run],
                                         "Data: "<>ToString[extrapOrder[run]],
                                         "-"]}}, Alignment -> Left]];

refConfigTimes =
{{"BAM/EOB_q1._0.5_0.1_D30D12.5162", 85.739734`8.384727125312157}, 
 {"GATech/Q2_a0.60_0.60", 112.532613`8.502823396773236}, 
 {"GATech/Q2_a-0.15_0.60", 120.712286`8.53329646798819}, 
 {"GATech/Q2_a0.30_0.00", 137.31628`8.58926702304901}, 
 {"GATech/Q1_a0.60_a0.60", 161.80675`8.660541628365586}, 
 {"Llama-AEI/Q1S6", 90.346969`8.40745858073432}, 
 {"LazEv-RIT/q10", 182.157994`8.711993228496961}, 
 {"SpEC-CCC/SpEC-Anti44", 76.131399`8.3331088040117}, 
 {"BAM/EP_um2D10_d6_sz", 64.850861`8.263460739915558}, 
 {"SpEC-CCC/SpEC-Case32", 216.290176`8.786581787526451}, 
 {"SpEC-CCC/SpEC-Aligned44", 96.441398`8.435808490714503}, 
 {"SpEC-CCC/SpEC-Case01", 217.453241`8.788910878424094}, 
 {"BAM-FAU/o.022e4q1S0.6_60_0_S0.6_120_90", 27.472511`7.890443349443861}, 
 {"Lean-Contrib/dq4", 56.120342`8.200665302471325}, 
 {"SpEC-CCC/SpEC-Case07a", 73.149105`8.315754010273924}, 
 {"SpEC-CCC-Contrib/SpEC-q1s0.00", 2.395631`6.830964917812059}, 
 {"SpEC-CCC-Contrib/SpEC-q2s0.00", 0.307359`5.939190928028204}, 
 {"SpEC-CCC-Contrib/SpEC-q3s0.00", 0.270903`5.884358807965206}, 
 {"SpEC-CCC-Contrib/SpEC-q4s0.00", 0.337898`5.9803306147129}, 
 {"BAM-FAU/om0.025etmq3S0.4_0_0_S0.6_0_0", 50.287359`8.153003821364967}, 
 {"SpEC-CCC-Contrib/SpEC-q6s0.00", 0.258012`5.863184898732789}, 
 {"SpEC-CCC-Contrib/SpEC-q1s+0.44", 0.127728`5.557831105422953}, 
 {"SpEC-CCC-Contrib/SpEC-q1s-0.95", 0.065568`5.268236930217066}, 
 {"SpEC-CCC-Contrib/SpEC-q1s-0.44", 0.08446`5.3781960705848615}, 
 {"SpEC-CCC-Contrib/SpEC-q1s+0.97", 0.08232`5.367050355250343}, 
 {"SpEC-CCC/SpEC-Case02", 244.373067`8.83959833296408}, 
 {"UIUC/q1_spin_0_and_spin_0.3_aligned", 15.558658`7.64351712803078}, 
 {"Lean/S3", 31.136786`7.9448187753150314}, 
 {"Llama-AEI/Q1S3", 68.690859`8.288443940890422}, 
 {"Llama-Palma-Caltech/q1a8a-4", 36.622704`8.015295400335628}, 
 {"Llama-AEI/Q1S-3", 45.200773`8.106690855448154}, 
 {"Llama-Palma-Caltech/q1a8a4", 39.568853`8.048898454856504}, 
 {"SpEC-CCC/SpEC-Case06", 330.974164`8.971339087361846}, 
 {"SpEC-CCC/SpEC-Case10", 375.689632`9.026374203392143}, 
 {"SpEC-CCC/SpEC-Case03", 486.668907`9.138778593798078}}

ARReport[] :=
  Module[{configReportPIDs, configReports, configReportsAndTimes, configTimes,
          exclude, configs, aborted, computeTime, job, jobs, remaining, result, results, wallTime},

    (* exclude = *)
    (*   {"BAM-FAU/om0.025etmq3S0.4_0_0_S0.6_0_0", *)
    (*    "Llama-AEI/Q1S-3", *)
    (*    "Llama-Palma-Caltech/q1a8a-4", *)
    (*    "Llama-Palma-Caltech/q1a8a4", *)
    (*    "GATech/Q1_a0.60_a0.60", "GATech/Q2_a0.60_0.60", "Llama-AEI/Q3T60S6", "SpEC-CCC/SpEC-Case07a", *)
    (*    "SpEC-CCC/SpEC-Case10"}; *)


    (* configs = {"Lean-Contrib/dq4"}; *)

    configs = NRARConfigurations[];

    configs = Reverse[Join[First/@Select[Sort[refConfigTimes /. 0. -> Infinity, #1[[2]] > #2[[2]] &],
                          MemberQ[configs, #[[1]]] &],
                   Select[configs,
                          (! MemberQ[First/@refConfigTimes, #]) &]]];

    (* configs = Select[configs, (!StringMatchQ[#, "SpEC-CCC-Contrib/*"]) &]; *)

    Print["Submitting configurations: "];
    Map[Print, configs];

    jobs = Map[Composition[ParallelSubmit,ReportOfConfiguration], configs];
    remaining = jobs;

    results = {};

    wallTime = 
    AbsoluteTiming[While[Length[remaining] =!= 0,
      Print[Length[remaining], " configurations remaining out of ", Length[jobs]];
      Print[Round[N[1-Length[remaining]/Length[jobs]]100.], " % complete"];
      Print["Waiting for ", remaining[[All,2,1]]];     
      {result, job, remaining} = WaitNext[remaining];
      If[result === $Aborted, result = {0., $Aborted}];
      Print["Finished: ", job[[2,1]]];
      AppendTo[results, Prepend[If[result === $Aborted, {0., $Aborted}, result],job[[2,1]]]]

      If[result[[2]] === $Aborted,
        Print["Aborted: ", job[[2,1]]]]]][[1]];

    results = results /. {c_, t_, $Aborted} -> {c, t, LaTeXSection[c, c, "Analysis aborted for unknown reason\n\n"]};

    (* configReportsAndTimes = ParallelMap[ReportOfConfiguration, configs, Method -> "FinestGrained"]; *)

    (* If[configReportsAndTimes === $Aborted, *)
    (*    Error["Report generation failed"]]; *)

    (* aborted = Position[results, $Aborted]; *)
    (* Print["aborted = ", aborted]; *)

    (* Print["Aborted configs were: ", Extract[results, aborted]]; *)

    (* configReportsAndTimes = Delete[configReportsAndTimes, aborted]; *)
    (* configs = Delete[configs, aborted]; *)

    (* configReports = configReportsAndTimes[[All,2]]; *)
    (* configTimes = configReportsAndTimes[[All,1]]; *)

    Put[results[[All,{1,2}]], "config-times.m"];

    computeTime = Plus@@results[[All,2]];

    Print["Wall time/s: ", wallTime];
    Print["Wall time/m: ", wallTime/60.];
    Print["Compute time/s: ", computeTime];
    Print["Utilisation: ", 100 computeTime/wallTime/$KernelCount, "%"];

    Print["Timings/s: \n"];
    Print[results[[All,{1,2}]]//TableForm];

    GenerateLaTeX["arreportcontent", ToLaTeX[Sort[results,OrderedQ[{#1[[1]],#2[[1]]}] &][[All,3]]]]];

(**************************)
(* AR Repository *)
(**************************)

Options[ReadAmplitudeError] = {AlignedErrors -> False};
ReadAmplitudeError[run_String, OptionsPattern[]] :=
 MakeDataTable@ReadColumnFile[run, "hAmpPhase_l2_m2.asc",
                              {1, If[OptionValue[AlignedErrors],6,4]}];

Options[ReadPhaseError] = {AlignedErrors -> False};
ReadPhaseError[run_String, OptionsPattern[]] :=
 MakeDataTable@ReadColumnFile[run, "hAmpPhase_l2_m2.asc",
                              {1, If[OptionValue[AlignedErrors],7,5]}];

ReadAmplitude[run_String] :=
 MakeDataTable@ReadColumnFile[run, "hAmpPhase_l2_m2.asc",
                              {1, 2}];

ReadPhase[run_String] :=
 MakeDataTable@ReadColumnFile[run, "hAmpPhase_l2_m2.asc",
                              {1, 3}];

ErrorReferenceTime[run_String] :=
 ReadMetadataKey[run, "error-reference-time"];

AlignmentWindow[run_String] :=
 ReadMetadataKey[run, "alignment-window"];

TruncationErrorAlignmentShifts[run_String] :=
 ReadMetadataKey[run, #] & /@ {"truncation-error-alignment-shift-t", 
   "truncation-error-alignment-shift-phi"};

ExtrapolationErrorAlignmentShifts[run_String] :=
 ReadMetadataKey[
    run, #] & /@ {"extrapolation-error-alignment-shift-t", 
   "extrapolation-error-alignment-shift-phi"};

FFIErrorAlignmentShifts[run_String] :=
 ReadMetadataKey[run, #] & /@ {"strain-conversion-error-alignment-shift-t", 
   "strain-conversion-error-alignment-shift-phi"};

InitialiseParallelNRARAnalysis[] :=
  Module[{x},
    (* DistributeDefinitions does not distribute $Path because it is
       in the System context.  Distributing
       SimulationTools`$SimulationPath does not work; it appears to
       work, but then does not seem to be found when used. *)
    (* DistributeDefinitions[$Path, SimulationTools`$SimulationPath]; *)

    (* This workaround allows us to assign the variables directly on
       the subkernels *)
    ReleaseHold[(Hold[ParallelEvaluate[SimulationTools`$SimulationPath = x]]
      /. x -> SimulationTools`$SimulationPath)];
    ReleaseHold[(Hold[ParallelEvaluate[$Path = x]]
      /. x -> $Path)];

    ParallelEvaluate[<< NRARAnalysis`];

    (* Initialise kernels sequentially to avoid running into OS
       process-spawning limits *)
    (* Do[ *)
    (*   Print["Initialising kernel ", i]; *)
    (*   (\* Pause[2]; *\) *)
    (*   ParallelEvaluate[<< NRARAnalysis`, i], *)
    (*   {i, 1, $ProcessorCount}] *)];

End[];

EndPackage[];
