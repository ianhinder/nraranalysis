
texfiles=$(wildcard *.tex)
basename=$(texfiles:.tex=)
texpdffiles=${texfiles:.tex=.pdf}

PDFLATEX = pdflatex -file-line-error -halt-on-error

$(texpdffiles):	%.pdf: %.tex $(pdffiles) $(bibfiles) $(pdffigs) $(includefiles)
		$(PDFLATEX) $<
		bibtex $(basename)
		$(PDFLATEX) $<
		$(PDFLATEX) $<

$(pdffiles):	%.pdf: %.eps
		epstopdf $<

$(epsfiles):	%.eps: %.gp
		gnuplot-eps -st $<

clean:
		rm -f $(epsfiles) $(pdffiles) falloff.pdf *.aux *.log
