\documentclass[aps,prd,amsmath,nofootinbib,10pt,showpacs,preprintnumbers,notitlepage]{revtex4}
\usepackage{graphicx}
\usepackage{color}
%\usepackage[retainorgcmds]{IEEEtrantools}
\usepackage{hyperref}
\usepackage{dcolumn}
\usepackage{multirow}
\usepackage{listings}
\usepackage{fancyhdr}

\lstset{breaklines=true}
\lstset{basicstyle=\ttfamily}
\definecolor{brickred}{rgb}{0.5,0,0}

\pagestyle{fancy}
\setlength{\headheight}{15.2pt}
\lhead{}
%% \rhead{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% New Commands
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\interfootnotelinepenalty=10000

\begin{document}

\raggedbottom
\renewcommand{\arraystretch}{1.2}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{AR Repository}

\author{Ian Hinder}
\email{ian.hinder@aei.mpg.de}
\affiliation{
Max-Planck-Institut f\"ur Gravitationsphysik,
Albert-Einstein-Institut, \\ 
Am M\"uhlenberg 1, D-14476 Golm, Germany}

\date{\today}

\begin{abstract}
This document contains an analysis of the accuracy of the data in the
NRAR NR repository.  It accompanies the AR repository, which contains
post-processed waveforms including error estimates.
\end{abstract}

\maketitle

\tableofcontents

\section{Strain waveforms}

All waveforms are computed from $\Psi_4$ data.  At each radius, $\Psi_4$ is
converted to strain using the FFI method using a cutoff which is half
the initial GW frequency.

The resulting strains are extrapolated in amplitude and phase by first
time-shifting with the Schwarzschild tortoise coordinate to account
for propagation time between different radii (the mass used is the ADM
mass) and then fitting to a polynomial dependence on radius.  The
largest 5 radii are used for the extrapolation.  The order, $p$, of
extrapolation is generally 2, but in some cases it is lower because
only a small number of radii were provided or because it was observed
that the extrapolation quality was poor with $p = 2$.  If only
infinite-radius CCE data is available, we use that instead of
extrapolation.

\section{Error analysis}

The following sources of error are estimated:
\begin{itemize}
\item Finite resolution error, for finite-difference codes, is
  estimated with Richardson extrapolation of the highest two
  resolutions at the expected convergence order, whereas for the
  spectral code, the difference between the highest two resolutions is
  used.
\item Finite radius error is estimated as the difference between $p$th
  and $(p+1)$th order extrapolation.
\item Conversion from $\Psi_4$ to strain using the FFI method contains
  errors from spectral leakage and too-high choice of cutoff frequency
  (see Reisswig and Pollney).  We generally expect the cutoff
  frequency to be high enough that spectral leakage is not an issue,
  and estimate the error due to choice of cutoff frequency by
  differentiating the strain twice to get $\Psi_4$, and comparing this
  with the original $\Psi_4$.
\end{itemize}

These errors are all computed separately in amplitude and phase, and
added together in quadrature to obtain the final amplitude and phase
errors.  The amplitude error is given as $\Delta A/A$ and the phase
error is given in radians as $\Delta \phi$.

For the purpose of calibrating the EOB model, errors can be computed
by applying a phase and time shift to the waveform in order to
minimise the phase difference in an ``alignment window'' which is the
window of time $[t_1,t_2]$ which will be used in the EOB calibration
procedure.  The three sources of error are each computed individually
using this alignment, and then combined in quadrature to give the
final aligned error estimate.

Both unaligned and aligned errors are plotted and provided in the data
files.

\section{Data format}

The post-processed waveforms are stored in the Numerical Relativity
Data Format (currently with some omissions and extensions).  The
waveform files are called \verb|hAmpPhase_l2_m2.asc| and contain the
following columns:

\begin{itemize}
\item \verb|t|: retarded time
\item \verb|A| : amplitude of $h$
\item \verb|phi| : phase of $h$
\item \verb|dA/A| : unaligned relative combined error in $A$
\item \verb|dphi| : unaligned absolute combined error in $\phi$
\item \verb|DA/A| : aligned relative combined error in $A$
\item \verb|Dphi| : aligned absolute combined error in $\phi$
\end{itemize}

The metadata files in the AR repository contain much of the same
information as those in the NR repository, with additional information
obtained during the error analysis.

\section{Plots}

The plots on the subsequent pages refer to the reference time,
$t_\mathrm{ref}$, which is the coordinate time at which the GW
frequency of the highest resolution waveform is $0.2/M$, as well as
the maximum time, which is the time at which $|h|$ is a maximum.  The
vertical dashed lines indicate the window of alignment for computing
aligned errors.

%\clearpage

\input{arreportcontent}

\end{document}
