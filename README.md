
NRARAnalysis
============

This repository contains the NR/AR analysis code used for analysing
the accuracy of numerical relativity (NR) waveforms and processing
them into a form suitable for comparison with analytical models and
other gravitational wave science.

NRARAnalysis was written by Ian Hinder and is provided as free
software according to the terms of the GNU General Public Licence
(GPL) v3.

Requirements
------------

* The code is written in Mathematica, and has been tested with both
  version 8 and version 9.

* The code uses [SimulationTools](http://simulationtools.org), a free
  software package for the analysis of numerical simulation data in
  Mathematica.  SimulationTools was developed by Ian Hinder and Barry
  Wardell.  Installation instructions for SimulationTools are included
  below.

* NRARAnalysis has been tested on Linux and Mac OS.  It may also work
  on Windows, but has not been tested there.

* The NR data to be analysed should be in the [Numerical Relativity Data
  Format](http://arxiv.org/abs/0709.0093).

Installing
----------

The code can be downloaded either using Git:

    :::text
    Mac OS: cd ~/Library/Mathematica/Applications
    Linux: cd ~/.Mathematica/Applications
    git clone https://bitbucket.org/simulationtools/simulationtools.git SimulationTools
    git clone https://bitbucket.org/ianhinder/nraranalysis.git NRARAnalysis

or as Zip files:

* Download NRARAnalysis
  [master.zip](https://bitbucket.org/ianhinder/nraranalysis/get/master.zip),
  extract it, and rename the extracted directory as NRARAnalysis.

* Download SimulationTools
  [master.zip](https://bitbucket.org/simulationtools/simulationtools/get/master.zip),
  extract it, and rename the extracted directory as SimulationTools.

* Move NRARAnalysis and SimulationTools into your Mathematica
  Applications directory (Mac OS: ~/Library/Mathematica/Applications,
  Linux: ~/.Mathematica/Applications)

If you already have SimulationTools installed, make sure to update it,
as NRARAnalysis requires the latest development version.

Data
----

The NR data format specifies how the data from a single NR simulation
should be provided.  The NRARAnalysis code processes sets of
simulations at different resolutions in order to compute error
estimates. Such a set is termed a "configuration", as it represents a
physical configuration.

NRARAnalysis expects the NR data to be arranged in a directory
structure of the form

    :::text
    data/group/configuration/resolution

where "group" is the name of the group or code that created the NR
data.

If you have access to the NRAR data repository, some example data can
be downloaded to a directory of your choice using

    :::text
    cd path/to/data
	
    for c in Llama-AEI/Q1S6 UIUC/q1_spin_0_and_spin_0.3_alignedv3; do
      svn checkout https://www.ninja-project.org/nrar/svn/data/$c $c
    done

Using
-----

### Mathematica notebook interface

An example Mathematica notebook showing how to run the analysis code
and visualise some of the output is available in
[NRARAnalysis/Examples/NRARExport.nb](nraranalysis/src/master/Examples/NRARExport.nb)
([web version](http://ianhinder.bitbucket.org/NRARAnalysis/NRARExport.html)).
The notebook shows how to run the analysis in parallel as well as
serial.

Open Mathematica, and load the NRARAnalysis package:

    :::text
    << NRARAnalysis`

Set the $SimulationPath variable to point to the data directory:

    :::text
    $SimulationPath = "path/to/data";

To process a configuration, use the function

    :::text
    ExportNRConfigurationAsAR["group/config", "path/to/export"]

### Command line interface

To run the analysis from the command line, there is a script
[NRARAnalysis/bin/nrarexport](nraranalysis/src/master/bin/nrarexport):

    :::text
    nrarexport path/to/data path/to/export/directory group/config

The script has more options; use nrarexport -h to see the
documentation.

### Cluster batch system

An example script for running the analysis on a cluster with a queuing system
is in
[NRARAnalysis/Examples/nrarexport.qsub](nraranalysis/src/master/Examples/nrarexport.qsub).

Output
------

In the export directory, a copy of the group/configuration directory
structure will be created.  The configuration directory will contain a
new metadata file, suitable for the processed data, and a file called
hAmpPhase_l2_m2.asc containing the processed data for the l = 2, m = 2
mode of the gravitational wave strain.

The columns of this file are:

Column        | Variable | Description
------------- | -------- | ---------------------------------------------
1             |  t       |  Retarded time                               
2             |  A       |  Amplitude of the strain mode
3             |  phi     |  Phase of the strain mode
4             |  dA/A    |  Relative error in the amplitude 
5             |  dphi    |  Absolute error in the phase 
6             |  DA/A    |  Relative error in the phase after alignment 
7             |  Dphi    |  Absolute error in the phase after alignment 

See the [NRAR paper](http://arxiv.org/abs/1307.5307) for a detailed
description of the meaning of these.


