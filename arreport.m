
SetOptions["stdout", PageWidth -> Infinity];

nProcs = 12;

If[$ProcessorCount == 24,
   Unprotect[$ProcessorCount];
   $ProcessorCount = nProcs;];

init[] :=
  ((* $NRMMADebug = True; *)

   $Path = Prepend[$Path, "."];
   Print["Loading SimulationTools from ", FindFile["SimulationTools`"]];
   Check[TimeConstrained[Get["SimulationTools`"],120,
                         Print["Kernel ",$KernelID," took more than 120s to load SimulationTools; quitting"]; Quit[]],
         Print["Aborting due to messages while loading SimulationTools"]; Abort[]];
   Print["SimulationTools load finished"];
   WithErrorChecking[{"NRARAnalysis`AnalyseRepo`", "NRARAnalysis`Configuration`", "NRARAnalysis`Extrapolation`", "NRARAnalysis`Utils`", "NRARAnalysis`Result`"},
                     Get["NRARAnalysis`"]];
   RunDirectory = "nrardata";
   dataDir = "nrardata";
   If[!FileExistsQ[dataDir], Print["Data directory ", dataDir, " not found"]; Quit[]];
   Module[
     {lockDir = FileNameJoin[{$TemporaryDirectory, "GZIP.exe.lock"}]},
     If[FileExistsQ[lockDir], DeleteDirectory[lockDir]]];
   NRARAnalysis`AnalyseRepo`ClearLog[];
  Print["SimulationTools loaded on kernel ", $KernelID," from ", FindFile["SimulationTools`"]];
  $KernelID);

init[];
DistributeDefinitions[init];
ParallelEvaluate[Print["Running on kernel ",$KernelID]];

Print["Setting inits"];
inits = ConstantArray[False, nProcs];

Do[
  Print["Initialising kernel ", i];
  inits[[i]] = ParallelEvaluate[init[], i],
  {i, 1, nProcs}];

Print["inits = ", inits];
If[Union[inits] =!= Table[i,{i,1,nProcs}],
   Print["At least one kernel failed to start; aborting"];
   Print[inits];
   Quit[]];

NRARAnalysis`AnalyseRepo`ARReport[];

Print["Done"];
