BeginPackage["NRARAnalysis`", {"SimulationTools`"}];

EndPackage[];

WithArgumentChecking["NRARAnalysis`*",
  WithStackTrace["NRARAnalysis`*",
    Get["NRARAnalysis`AnalyseRepo`"];
    Get["NRARAnalysis`Configuration`"];
    Get["NRARAnalysis`Export`"];
    Get["NRARAnalysis`Extrapolation`"];
    Get["NRARAnalysis`LaTeX`"];
    Get["NRARAnalysis`Result`"];
    Get["NRARAnalysis`Utils`"];
    Get["NRARAnalysis`ARCompare`"]]];
