
BeginPackage["NRARAnalysis`Export`", {
  "NRARAnalysis`AnalyseRepo`",
  "NRARAnalysis`Configuration`",
  "NRARAnalysis`Export`",
  "NRARAnalysis`Extrapolation`",
  "NRARAnalysis`LaTeX`",
  "NRARAnalysis`Result`",
  "NRARAnalysis`Utils`",
  "SimulationTools`ColumnFile`",
  "SimulationTools`Convergence`",
  "SimulationTools`DataRepresentations`",
  "SimulationTools`DataTable`",
  "SimulationTools`Error`",
  "SimulationTools`Memo`",
  "SimulationTools`MessageCatcher`",
  "SimulationTools`NRDF`",
  "SimulationTools`NRExport`",
  "SimulationTools`NR`",
  "SimulationTools`Plotting`",
  "SimulationTools`ReadHDF5`",
  "SimulationTools`RunFiles`",
  "SimulationTools`Waveforms`"}];

ExportNRConfigurationAsAR::usage = "ExportNRConfigurationAsAR[config, dir] exports the NR configuration config, consisting of a set of simulations at different resolutions, as an AR configuration in directory dir";

Begin["`Private`"];

(* ------------------------------------------------------------------------ 
   ExportNRConfigurationAsAR
   ------------------------------------------------------------------------ *)

ExportNRConfigurationAsAR[config_String, exportDir_String] :=
  ExportNRConfigurationAsAR[RunsInConfiguration[config], exportDir];

ExportNRConfigurationAsAR[runs_List, exportDir_String] :=
  Module[
    {run, psi4, om, strain, strainAbs, strainPhase, outDir, data,
     config, phaseError, ampError,
     phaseTruncErr, phaseExtrapErr, phaseFFIErr,
     ampTruncErr, ampExtrapErr, ampFFIErr, int, tables,
     ampErrUnaligned, phaseErrUnaligned, header},

    WithCaughtMessages@Block[{$Memoisation = True,$EnableBuiltInHDF5Reader = True},

    log["Exporting config of runs ",runs];

    run = Last[runs];
    strain = FinalStrain[run];

    int[d_DataTable] :=
    Interpolation[d,InterpolationOrder -> 8];
    
    {{ampTruncErr, ampExtrapErr, ampFFIErr, ampError},
     {phaseTruncErr, phaseExtrapErr, phaseFFIErr, phaseError}} = StrainErrors[runs,True];

    {ampErrUnaligned, phaseErrUnaligned} = Map[Last, StrainErrors[runs,False]];

    (* Ensure that all the data is compatible *)
    tables = {strain, phaseTruncErr, phaseExtrapErr, phaseFFIErr, phaseError, ampTruncErr, ampExtrapErr, ampFFIErr, ampError, ampErrUnaligned, phaseErrUnaligned} = 
    ResampleDataTables@IntersectDataTables[{strain, phaseTruncErr, phaseExtrapErr, phaseFFIErr, phaseError, ampTruncErr, ampExtrapErr, ampFFIErr, ampError, ampErrUnaligned, phaseErrUnaligned}];

    (* Take care of units *)
    M = ReadTotalMass[run];
    {strain, phaseTruncErr, phaseExtrapErr, phaseFFIErr, phaseError, ampTruncErr, ampExtrapErr, ampFFIErr, ampError, ampErrUnaligned, phaseErrUnaligned} = 
    (* TODO: Why are the amplitude errors divided by M?  They are percentages. *)
    ScaledCoordinate[#,1/M] & /@ {strain/M,
                                  phaseTruncErr, phaseExtrapErr, phaseFFIErr, phaseError,
                                  ampTruncErr/M, ampExtrapErr/M, ampFFIErr/M, ampError/M,
                                  ampErrUnaligned/M, phaseErrUnaligned};

    strainAbs = Abs[strain];
    strainPhase = Phase[strain];

    config = FileNameTake[FileNameDrop[runs[[1]],-1],-2];

    outDir = FileNameJoin[{exportDir,config}];
    ensureDirectory[outDir];

    log["Arranging data"];
    data = Thread[{IndVar[strainAbs], DepVar[strainAbs], DepVar[strainPhase],
                   0.01 DepVar[ampErrUnaligned], DepVar[phaseErrUnaligned], (* , *)
                   0.01 DepVar[ampError], DepVar[phaseError] (* , *)
                   (* ampTruncErr/@IndVar[strain], phaseTruncErr/@IndVar[strain], *)
                   (* ampExtrapErr/@IndVar[strain], phaseExtrapErr/@IndVar[strain] *)}];
    log["Writing exported file to ", outDir];

    header = {"#","1:A","2:phi","3:dA/A","4:dphi","5:DA/A","6:Dphi"};

    Export[FileNameJoin[{outDir, "hAmpPhase_l2_m2.asc"}], Prepend[data,header], {"TABLE"}];
    exportMetadata[exportDir, config]]];

(* ------------------------------------------------------------------------ 
   exportMetadata
   ------------------------------------------------------------------------ *)

exportMetadata[exportDir_String, config_String] :=
  Module[{copyKeys, processKeys, md, run, allmd, MKeys, M2Keys, M},

  run = HighResolutionRun[config];

  copyKeys = {"data-type",
              "simulation-bibtex-keys",
              "comments",
              "documentation",
              "authors-emails",
              "code",
              "code-version",
              "code-bibtex-keys",
              "evolution-system",
              "evolution-gauge",
              "resolution-expected-order",
              "initial-data-type",
              "initial-data-bibtex-keys",
              "quasicircular-bibtex-keys",
              "eccentricity-measurement-method",
              "submitter-email",
              "eccentricity",
              "freq-start-22",
              "number-of-cycles-22",
              "phase-error",
              "amplitude-error-relative"};

  M = ReadTotalMass[run];

  MKeys = {   "initial-ADM-energy",
              "initial-separation",
              "after-junkradiation-time",
              "mass1",
              "mass2",
              "initial-bh-position1x",
              "initial-bh-position1y",
              "initial-bh-position1z",
              "initial-bh-position2x",
              "initial-bh-position2y",
              "initial-bh-position2z",
              "initial-bh-momentum1x",
              "initial-bh-momentum1y",
              "initial-bh-momentum1z",
              "initial-bh-momentum2x",
              "initial-bh-momentum2y",
              "initial-bh-momentum2z"};

  M2Keys =  { "initial-angular-momentumx",
              "initial-angular-momentumy",
              "initial-angular-momentumz",
              "initial-bh-spin1x",
              "initial-bh-spin1y",
              "initial-bh-spin1z",
              "initial-bh-spin2x",
              "initial-bh-spin2y",
              "initial-bh-spin2z",
              "after-junkradiation-spin1x",
              "after-junkradiation-spin1y",
              "after-junkradiation-spin1z",
              "after-junkradiation-spin2x",
              "after-junkradiation-spin2y",
              "after-junkradiation-spin2z"};

  md = Join[Map[# -> ReadMetadataKey[run,#] &, Select[copyKeys,HaveMetadataKey[run,#] &]],
            Map[# -> ReadMetadataKey[run,#]/M &, Select[MKeys,HaveMetadataKey[run,#] &]],
            Map[# -> ReadMetadataKey[run,#]/M^2 &, Select[M2Keys,HaveMetadataKey[run,#] &]]];

  md = Join[md, {"error-reference-time" -> ReferenceTimeFromFinalStrain[run]/M}];

  If[HaveRunsForTruncationError[config],
    md = Join[md, {"alignment-window" -> ExportList@(AlignmentWindowCCC[config]/M),
                  "truncation-error-alignment-shift-t" -> (truncationErrorAlignmentShifts[config,True][[1]]/M),
                  "truncation-error-alignment-shift-phi" -> truncationErrorAlignmentShifts[config,True][[2]]}]];

  (* This logic is not quite right, but is good enough for now *)
  If[!HaveInfiniteRadiusWaveforms[run],
    md = Join[md, {"radius-extrapolation-order" -> extrapOrder[run],
                   "extrapolation-radii" -> ExportList@extrapRadiiForRun[run],
                   "extrapolation-error-alignment-shift-t" -> (extrapolationErrorAlignmentShifts[run,True][[1]]/M),
                   "extrapolation-error-alignment-shift-phi" -> extrapolationErrorAlignmentShifts[run,True][[2]]}]];

  md = Join[md, {"psi4-to-strain-ffi-cutoff" -> (M strainFrequency[run]),
                 "ffi-error-alignment-shift-t" -> (ffiErrorAlignmentShifts[run,True][[1]]/M),
                 "ffi-error-alignment-shift-phi" -> ffiErrorAlignmentShifts[run,True][[2]]}];

  md = md /. 
         {NameEmailList[nes___] :> StringJoin[Riffle[
           Map[#[[1]]<>" <"<>#[[2]]<>">" &, {nes}], ", "]],
          EmailList[es___] :> StringJoin[Riffle[{es}, ", "]],
          n_?NumberQ :> ToString[n,CForm]};

  allmd = {"metadata" -> md};

  If[FileType[FileNameJoin[{exportDir,config}]] === None,        
    CreateDirectory[FileNameJoin[{exportDir,config}], CreateIntermediateDirectories->True]];

  ExportMetadata[FileNameJoin[{exportDir,config,FileNameTake[config,-1]<>".bbh"}], allmd]];

End[];

EndPackage[];
