(* Copyright 2014 Ian Hinder

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

BeginPackage["NRARAnalysis`Configuration`", {"SimulationTools`", "SimulationTools`NRDF`", "SimulationTools`Error`"}];

ConfigOfRun;
ReadConfigurations;
RunsInConfiguration;
NRARConfigurations;
strainFrequency;
ConfigurationOfRun;
HighResolutionRun;
LowResolutionRun;
NRARConfigurationLabel;

Begin["`Private`"];

ConfigOfRun[run_String] :=
  FileNameSplit[run][[{-3,-2}]];

ConfigurationOfRun[run_String] :=
  FileNameJoin[FileNameSplit[run][[{-3,-2}]]];

HighResolutionRun[config_String] :=
  Module[
    {runs = RunsInConfiguration[config]},
    If[Length[runs] === 0, Error["Configuration "<>config<>" does not contain any runs"]];
    Return[Last[runs]]];

LowResolutionRun[config_String] :=
  Module[
    {runs = RunsInConfiguration[config]},
    If[Length[runs] === 0, Error["Configuration "<>config<>" does not contain any runs"]];
    If[Length[runs] === 1, Error["Configuration "<>config<>" only contains one resolution"]];
    Return[runs[[-2]]]];

ReadConfigurations[] :=
 Union[Map[FileNameDrop[#, -1] &, ReadRuns[]]];

RunsInConfiguration[config_String] :=
 Module[{runs = Select[ReadRuns[], FileNameDrop[#, -1] == config &]},
  runs[[Ordering[ReadResolution /@ runs]]]];

strainFrequency[run_] :=
  0.5*StartingFrequency[run];

(* Supplied by Mark Hannam on 28.05.2013 *)
$configNameTable = {{1, "J1p+49+11", 
   "BAM/EOB_q1._0.5_0.1_D30D12.5162"}, {3, "F1p+30-30", 
   "BAM-FAU/o.022e4q1S0.6_60_0_S0.6_120_90"}, {21, "C3p+00-15", 
   "SpEC-CCC/SpEC-Case06"}, {22, "C1p+30+30", 
   "SpEC-CCC/SpEC-Case07a"}, {23, "C1p-30-30", 
   "SpEC-CCC/SpEC-Case10"}, {2, "J2+60-15", 
   "BAM/EP_um2D10_d6_sz"}, {4, "F3+40+60", 
   "BAM-FAU/om0.025etmq3S0.4_0_0_S0.6_0_0"}, {5, "G1+60+60", 
   "GATech/Q1_a0.60_a0.60"}, {6, "G2-60+15", 
   "GATech/Q2_a-0.15_0.60"}, {7, "G2+00+30", 
   "GATech/Q2_a0.30_0.00"}, {8, "G2+60+60", 
   "GATech/Q2_a0.60_0.60"}, {9, "R10+00+00", "LazEv-RIT/q10"}, {10, 
   "L4+00+00", "Lean-Contrib/dq4"}, {11, "L3+60+00", "Lean/S3"}, {12, 
   "A1+30+00", "Llama-AEI/Q1S3"}, {13, "A1+60+00", 
   "Llama-AEI/Q1S6"}, {14, "P1+80-40", 
   "Llama-Palma-Caltech/q1a8a-4"}, {15, "P1+80+40", 
   "Llama-Palma-Caltech/q1a8a4"}, {16, "C1+44+44", 
   "SpEC-CCC/SpEC-Aligned44"}, {17, "C1-44-44", 
   "SpEC-CCC/SpEC-Anti44"}, {18, "C1+30+30", 
   "SpEC-CCC/SpEC-Case01"}, {19, "C2+30+30", 
   "SpEC-CCC/SpEC-Case02"}, {20, "C3+30+30", 
   "SpEC-CCC/SpEC-Case03"}, {24, "C3-60+00", 
   "SpEC-CCC/SpEC-Case32"}, {25, "U1+00+30", 
   "UIUC/q1_spin_0_and_spin_0.3_alignedv3"}};

$configNameRules = Map[#[[3]] -> {#[[1]], #[[2]]} &, $configNameTable];

NRARConfigurationLabel[config_String] :=
  If[!MemberQ[$configNameTable[[All,3]], config],
     Error["Configuration "<>config<>" not found in name lookup table"],
     config /. $configNameRules];

NRARConfigurations[] :=
  Module[{exclude,configs},
    exclude = {"Llama-AEI/Q3T60S6", "Llama-AEI/Q1S-3", "UIUC/q1_spin_0_and_spin_0.3_aligned"};
    configs = ReadConfigurations[];                                            
    configs = Select[configs,(!MemberQ[exclude,#]) &]];

(* NRARConfigurations[] := *)
  (* Select[ReadConfigurations[], StringMatchQ[#, "BAM/*"] &]; *)
  (* {"UIUC/q1_spin_0_and_spin_0.3_aligned"}; *) (*, "UIUC/q1_spin_0_and_spin_0.3_aligned-new_grids"}; *)
(* (\* {"LazEv-RIT/q10"}; *\) *)
  (* {"Llama-AEI/Q1S6"}; *)
(* {"Llama-AEI/Q1S-3"}; *)
(* {"UIUC/orig","UIUC/newrads","UIUC/newgrids"}; *)
(* {"GATech/Q2_a0.30_0.00_v2"}; *)
(* {"UIUC/q1_spin_0_and_spin_0.3_aligned"}; *)

End[];

EndPackage[];
