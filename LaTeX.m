(* Copyright 2014 Ian Hinder

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

BeginPackage["NRARAnalysis`LaTeX`", {"SimulationTools`", "SimulationTools`Error`", "NRARAnalysis`Utils`"}];

FlattenBlock;
GenerateFile;
GenerateLaTeX;
GraphicsFile;
LaTeXCatch;
LaTeXSection;
LaTeXSectionDepth;
ToLaTeX;
ExportList;
(* Section; *)

Begin["`Private`"];

FlattenBlock[b_] :=
  Module[
    {flattenBlock},
    flattenBlock[x_String] := x;
    flattenBlock[l_List] := StringJoin@@Map[FlattenBlock, l];
    flattenBlock[a_?AtomQ] := ToString[a];
    flattenBlock[n_?NumberQ] := ToString[n];
    flattenBlock[n_NumberForm] := ToString[n];
    flattenBlock[g_GraphicsFile] := "";
    flattenBlock[x_] := Throw["Do not know how to flatten "<>ToString[Short[x],InputForm]];

    flattenBlock[b]];

GenerateFile[filename_String, contents_] :=
  Module[
    {fp = OpenWrite[filename]},
    (* CheckBlock[contents]; *)
    WriteString[fp, FlattenBlock[contents]];
    Close[fp]];

(*************************************************************************************************)
(* LaTeX *)
(*************************************************************************************************)

latexRadius[Infinity] := "$\\infty$";

latexRadius[x_?NumberQ] := ToString[Round@x];

latexRadius[x_String] := x;

includeGraphics[file_] :=
  "\\includegraphics{" <> file <> "}\n";

figure[contents_] :=
 {"\\begin{figure}\n",
  contents,
  "\\end{figure}"}

(* LaTeXSection[title_, contents_] := *)
(*   {"\\section{" <> title <> "}\n\n", *)
(*    contents}; *)

subSection[title_, contents_] :=
 {"\\subsection{" <> title <> "}\n\n",
  contents}

subSubSection[title_, 
  contents_] :=
 {"\\subsubsection{" <> title <> "}\n\n",
  contents}

escape[s_String] :=
 StringReplace[s, "_" -> "\\_"];

escapeFileName[s_String] :=
 StringReplace[s, "_" -> "-"];

toFileName[label_] :=
  ToString[label];

SetAttributes[LaTeXCatch, HoldFirst];
ArgumentChecker`StandardDefinition[LaTeXCatch] = True;

LaTeXCatch[expr_] :=
  Catch[expr, _ErrorTag|_ErrorString, 
   Function[{value,tag}, 
     Print["LaTeXCatch: Caught error"];
     Module[{msg},
     msg = {"Error:", If[Head[tag]===ErrorTag,
                      ToString[HoldForm[tag]/.HoldPattern[ErrorTag[t_]]:>t]<>": "<>ToString@StringForm[ToString[tag[[1]]], Sequence@@value[[1]]],
                      tag[[1]]],
                    "\n",
                    Riffle[StackStringList[value[[2]]],"\n"]};
     Print[FlattenBlock[msg]];
     ListingBlock[msg]]]];

Options[ToLaTeX] = {"LaTeXSectionDepth" -> 1, "Path" -> {"arreportcontent-graphics"}};

ToLaTeX[Section[name_String, text_, content_], 
  opts : OptionsPattern[]] :=
 {"\\" <> {"section", "subsection", 
     "subsubsection"}[[OptionValue["LaTeXSectionDepth"]]] <> "{" <> 
   escape@text <> "}\n\n"
  , ToLaTeX[content, 
   LaTeXSectionDepth -> OptionValue["LaTeXSectionDepth"] + 1, 
   Path -> Append[OptionValue["Path"], name]]};

ToLaTeX[gf : GraphicsFile[name_String, g_Graphics], 
  opts : OptionsPattern[]] :=
  Module[{file = FileNameJoin@Append[OptionValue[Path], name]},
  {"\\includegraphics[width=\\textwidth]{" <> escapeFileName@file <> "}\n",
   GraphicsFile[escapeFileName@file, g]}];

ToLaTeX[x_List, opts:OptionsPattern[]] :=
  Map[ToLaTeX[#,opts] &, x];

ToLaTeX[x_String, opts:OptionsPattern[]] :=
  x;

ToLaTeX[VerbatimBlock[b_], opts:OptionsPattern[]] :=
  {"\\begin{verbatim}\n",
   ToLaTeX[b,opts],
   "\n\\end{verbatim}\n"};

ToLaTeX[ListingBlock[b_], opts:OptionsPattern[]] :=
  {"\\begin{lstlisting}\n",
   ToLaTeX[b,opts],
   "\n\\end{lstlisting}\n"};

ToLaTeX[g_Grid, opts:OptionsPattern[]] :=
  {"\\begin{displaymath}\n",
  ToString[g, TeXForm],
   "\n\\end{displaymath}\n"};

ToLaTeX[s_String, opts:OptionsPattern[]] :=
  s;

GenerateLaTeX[filename_String, l_] :=
 Module[{files},
  GenerateFile[filename <> ".tex", l];
  files = Cases[l, g_GraphicsFile, Infinity];
  Do[
   Module[{gfile = FileNameJoin[{f[[1]]}]},
    Quiet[
     (* Print["Creating directory ", FileNameDrop[gfile, -1]]; *)
     CreateDirectory[FileNameDrop[gfile, -1], 
      CreateIntermediateDirectories -> True], 
     CreateDirectory::filex];
    (* Print["Exporting graphics file ", gfile]; *)
    Export[gfile, f[[2]]]],
   {f, files}]];

ExportList[l_List] := StringJoin[Riffle[ToString/@l," "]]

End[];

EndPackage[];
