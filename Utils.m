(* Copyright 2014 Ian Hinder

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

BeginPackage["NRARAnalysis`Utils`", {"SimulationTools`", "SimulationTools`Error`", "SimulationTools`DataTable`", "SimulationTools`DataRepresentations`"}];

log;
ClearLog;
ensureDirectory;
fPrint;
ScaledCoordinate;
VerboseParallelMap;

Begin["`Private`"];

ArgumentCheckers`StandardDefinition[messageHandler] = True;

allowedMessages = {FindMaximum::eit, General::stop (*, InterpolatingFunction::dmval, FindRoot::lstol,
                   FindRoot::cvmit *) };

messageHandler[x_] :=
 If[Last[x] && !MemberQ[allowedMessages,x[[1,1]]],
  x /. HoldPattern[_[Message[id_, args___], _]] :> ErrorMessage[id, args]; Abort[]];

(* Internal`AddHandler["Message", messageHandler]; *)

log[args__] :=
  Module[
    {s,f},
    s = StringJoin@@(ToString/@{args});
    f = If[!ValueQ[NRARAnalysis`AnalyseRepo`$CurrentConfiguration],
          If[FileType["kernellogs"] === None, CreateDirectory["kernellogs"]];
          ToString@StringForm["kernellogs/log.`1`.txt",$KernelID],
          (* else *)
          If[FileType["configlogs"] === None, Quiet[CreateDirectory["configlogs"]]];
          ToString@StringForm[
            "configlogs/`1`.log",
            StringReplace[NRARAnalysis`AnalyseRepo`$CurrentConfiguration,"/"->"_"]]];
    PutAppend[s, f]];

ClearLog[] :=
  Put["", ToString@StringForm["log.`1`.txt",$KernelID]];

ensureDirectory[d_String] :=
  If[!FileExistsQ[d], CreateDirectory[d, CreateIntermediateDirectories -> True]];

fPrint[desc_String,x_] := (Print[desc,": ",x]; x);

ScaledCoordinate[d : DataTable[__], a_?NumberQ] :=
 AddAttributes[MakeDataTable[Map[{#[[1]] a, #[[2]]} &, ToList[d]]], 
  ListAttributes[d]];

formatState[s_] :=
 Replace[s,
  {Parallel`Developer`running[kernel_] :> "R",
   Parallel`Developer`queued :> "Q",
   (Parallel`Developer`dequeued | 
       Parallel`Developer`finished)[{_, $Aborted}] :> "E",
   Parallel`Developer`dequeued[result_] :> "F",
   Parallel`Developer`finished[result_] :> "F",
   x_ :> Error["Unknown process state " <> ToString[x]]}]

VerboseParallelMap[f_, l_List] := 
 Module[{eids, result, job, results, g, alleids, printStates, r}, 
  CheckAbort[
   If[Length[l] === 0, Return[{}]];
   alleids = 
    eids = MapIndexed[
      ParallelSubmit[{Print[DateString[], ": ", $KernelID, 
           ": Starting ", #], {#2[[1]], 
           r = CheckAbort[f[#], $Aborted]}, 
          Print[DateString[], ": ", $KernelID, ": Finished ", #, " (",
            r, ")"]}[[2]]] &, l];
   results = {};
   
   printStates[] :=
    Module[{states = 
       formatState /@ (Parallel`Developer`ProcessState /@ alleids)},
     
     Print[DateString[], ": ", $KernelID, ": ", Count[states, "R"], 
      " running, ", Count[states, "Q"], " queued, ", 
      Count[states, "F"], " finished, ", Count[states, "E"], 
      " errors (", 
      Round[100 (Count[states, "F"] + Count[states, "E"])/Length[l]], 
      "% complete)"]];
   
   While[Length[eids] > 0,
    printStates[];
    {result, job, eids} = WaitNext[eids];
    results = Append[results, result]];
   printStates[];
   
   Map[Last, SortBy[results, First]],
   (*On abort*)AbortKernels[]; Abort[]]];

End[];

EndPackage[];
