(* Copyright 2014 Ian Hinder

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

BeginPackage[
  "NRARAnalysis`Extrapolation`", 
  {"SimulationTools`", 
   "NRARAnalysis`Configuration`", 
   "SimulationTools`Memo`",
   "SimulationTools`DataTable`",
   "SimulationTools`Error`",
   "SimulationTools`CoordinateTransformations`",
   "SimulationTools`InitialData`",
   "SimulationTools`Plotting`",
   "SimulationTools`NRDF`",
   "SimulationTools`Waveforms`"}];

ConfigRadiusRange["Llama-AEI/Q1S6"] = {100,214}; (* Higher radii
                                                    ringdowns are
                                                    missing for the
                                                    second-highest
                                                    resolution as the
                                                    run was stopped
                                                    too early *)

ConfigRadiusRange["Llama-AEI/Q3T60S6"] = {100,115};
ConfigRadiusRange["BAM-FAU/o.022e4q1S0.6_60_0_S0.6_120_90"] = {70, 90};

ConfigRadiusRange["UIUC/q1_spin_0_and_spin_0.3_aligned"] = {70,192};

ConfigSkipRadii["SpEC-CCC/SpEC-Case02"] = {126., 198.};
ConfigSkipRadii[_String] = {};

ConfigExtrapolationOrder[_] = 0;

ConfigExtrapolationOrderMap = 
{"BAM/EOB_q1._0.5_0.1_D30D12.5162" -> {{0, 1, 1}, {0, 2, 3}}, 
 "BAM/EP_um2D10_d6_sz" -> {{0, 1, 2}, {0, 1, 2}}, 
 "BAM-FAU/o.022e4q1S0.6_60_0_S0.6_120_90" -> {{0, 1, 1}, {0, 1, 1}}, 
 "BAM-FAU/om0.025etmq3S0.4_0_0_S0.6_0_0" -> {{0, 2, 2}, {0, 2, 3}}, 
 "GATech/Q1_a0.60_a0.60" -> {{0, 2, 2}, {0, 2, 3}}, 
 "GATech/Q2_a-0.15_0.60" -> {{0, 2, 2}, {0, 2, 3}}, 
 "GATech/Q2_a0.30_0.00" -> {{0, 2, 2}, {0, 2, 3}}, 
 "GATech/Q2_a0.60_0.60" -> {{0, 2, 2}, {0, 2, 3}}, 
 "LazEv-RIT/q10" -> {{0, 1, 1}, {0, 1, 1}}, 
 "Lean-Contrib/dq4" -> {{0, 2, 3}, {0, 2, 3}}, 
 "Lean/S3" -> {{0, 1, 2}, {0, 1, 3}}, 
 "Llama-AEI/Q1S3" -> {{0, 2, 3}, {0, 1, 3}}, 
 "Llama-AEI/Q1S6" -> {{0, 2, 3}, {0, 1, 3}}, 
 "SpEC-CCC/SpEC-Aligned44" -> {{0, 3, 3}, {0, 1, 3}}, 
 "SpEC-CCC/SpEC-Anti44" -> {{0, 2, 3}, {0, 1, 3}}, 
 "SpEC-CCC/SpEC-Case01" -> {{0, 3, 3}, {0, 1, 3}}, 
 "SpEC-CCC/SpEC-Case02" -> {{0, 1, 1}, {0, 1, 1}}, 
 "SpEC-CCC/SpEC-Case03" -> {{0, 2, 3}, {0, 2, 3}}, 
 "SpEC-CCC/SpEC-Case06" -> {{0, 3, 3}, {0, 1, 3}}, 
 "SpEC-CCC/SpEC-Case07a" -> {{0, 2, 3}, {0, 2, 3}}, 
 "SpEC-CCC/SpEC-Case10" -> {{0, 3, 3}, {0, 2, 3}}, 
 "SpEC-CCC/SpEC-Case32" -> {{0, 2, 3}, {0, 2, 3}}, 
 "UIUC/q1_spin_0_and_spin_0.3_alignedv3" -> {{0, 1, 1}, {0, 1, 1}}};

(* ConfigRadiusRange["BAM/EOB_q1._0.5_0.1_D30D12.5162"] = {0,100}; *)
ConfigRadiusRange["BAM/EP_um2D10_d6_sz"] = {0,180};

defaultExtrapolationOrder = 2;

High;
Low;
extrapOrder;
extrapRadiiForConfig;
extrapRadiiForRun;
radiiInConfiguration;
extrapPlot;
strainsAtRadii;
extrapolationPlot;
ConfigRadiusCount;
ConfigExtrapolationOrder;

Begin["`Private`"];

(**************************************************************************************)
(* radiiInConfiguration *)
(**************************************************************************************)

radiiInConfiguration[config_String] :=
  Intersection@@(ReadPsi4Radii /@ RunsInConfiguration[config]);

(**************************************************************************************)
(* extrapRadiiForConfig *)
(**************************************************************************************)

extrapRadiiForConfig[config_String] :=
  Module[
    {rads, range, configRange, nc, c},
    rads = Complement[radiiInConfiguration[config], {Infinity}];

    If[Length[rads] === 0, Error["No Psi4 radii found in "<>config]];

    configRange = ConfigRadiusRange[FileNameJoin[config]];

    If[ListQ[configRange],
       rads = Select[rads, (configRange[[1]] <= # <= configRange[[2]]) &]];

    rads = Complement[rads, ConfigSkipRadii[config]];

    log["Extrapolation radii (", config, "): ", rads];
    rads];

(**************************************************************************************)
(* extrapRadiiForRun *)
(**************************************************************************************)

extrapRadiiForRun[run_String] :=
  extrapRadiiForConfig[FileNameJoin[ConfigOfRun[run]]];

(**************************************************************************************)
(* extrapOrder *)
(**************************************************************************************)

extrapOrder[run_String] :=
  Module[
    {nRadii, config, code, variant, phaseOrd, ampOrd, p},
    nRadii = Length[extrapRadiiForRun[run]];

    Switch[
      nRadii,
      0, Error["No finite-radius waveform data found in "<>run],
      1, Error["Only one finite-radius waveform found in "<>run<>
               ". This is not enough to compute an error estimate."],
      _, Null];

    config = ConfigurationOfRun[run];

    {code,variant} = StringSplit[config,"/"];

    phaseOrd =
    If[MemberQ[{"Llama-AEI","SpEC-CCC","SpEC-CCC-Contrib"}, code],
       2,
       If[MemberQ[{"BAM-FAU/o.022e4q1S0.6_60_0_S0.6_120_90"}, config],
          1,
          1]];

    ampOrd =
       If[MemberQ[{"BAM-FAU/o.022e4q1S0.6_60_0_S0.6_120_90","LazEv-RIT/q10","Lean/S3"},
                  config],
          0,
          2];
    p = {ampOrd, phaseOrd};
    log["Extrapolation order: ", p];
    p];

DefineMemoFunction[strainsAtRadii[run_, rs_, u_,adjusted_:False],
 Module[{MADM, om0, hs, fOfu, readStrain},
  MADM = ReadADMMass[run];
  om0 = strainFrequency[run];
  readStrain[r_] :=
    Module[
      {rpsi4 = r ReadPsi4[run, 2, 2, r]},
      If[adjusted, rpsi4 = Psi4PerturbativeCorrection[rpsi4,2,r,om0]];
      StrainFromPsi4[rpsi4,om0]];

  hs = ToRetardedTime[{rs, readStrain /@ rs}, RadialToTortoise[#, MADM] &];
  fOfu = (Interpolation[DataTableInterval[#, {u - 10, u + 10}]][
       u]) & /@ hs]];

Options[extrapolationPlot] = Join[Options[PresentationListLinePlot], {"Adjustment" -> False}];
extrapolationPlot[run_String, what_Symbol, u_, opts : OptionsPattern[]] :=
 Module[{allRads, useRads, MADM, M, F, hs, amps, uRef, fs, data, fMax,
    model, fit1, fit2, fit3, fit4, fit5, colours, om0, fOfu, fOfx, 
   usefOfx, usePositions, params, fit, xMax, curve, a, x, p, 
   ignorefOfx, which, ps, lists, ticks, fn},
  allRads = Sort[ReadPsi4Radii[run]];
  useRads = extrapRadiiForRun[run];
  usePositions = Flatten[Position[allRads, #] & /@ useRads];
  ps = Table[extrapOrder[run, which], {which, {Low, High}}];
  If[what === Abs, ps = Map[First, ps]];
  If[what === Arg, ps = Map[Last, ps]];
  AppendTo[ps, Last[ps] + 1];
  fOfu = what /@ strainsAtRadii[run, allRads, u, OptionValue[Adjustment]];
  fOfx = Thread[{1/allRads, fOfu}];
  usefOfx = fOfx[[usePositions]];
  ignorefOfx = Delete[fOfx, List /@ usePositions];
  xMax = Max[1/allRads];
  fMax = 1;
  model[p_] := Sum[a[i] x^i, {i, 0, p}];
  params[p_] := Table[a[i], {i, 0, p}];
  fit[p_] := FindFit[usefOfx, model[p], params[p], x];
  fit[0] := {a[0] -> usefOfx[[-1,2]]};
  curve[p_] := (model[p] /. fit[p]);
  colours = {Blue, Magenta, Darker@Green, Brown, Gray};
  lists = {MakeDataTable[usefOfx]};
  If[Length[ignorefOfx] > 0, 
   AppendTo[lists, MakeDataTable[ignorefOfx]]];
  M = ReadTotalMass[run];
  fn = If[what === Abs, "A", "\[Phi]"];
  
  ticks = 
   Table[{x, Rational[1, Round[1/(M x)]]}, {x, xMax/10, xMax, 
     xMax/10}];

  Show[PresentationListLinePlot[lists, FilterRules[{opts},Options[PresentationListLinePlot]], 
    PlotRange -> {{0, All}, All}, Joined -> False, 
    PlotMarkers -> Automatic, 
    FrameLabel -> {"1/r", StringForm[fn <> "(`1`)", u]}, 
    FrameTicks -> {{Automatic, None}, {ticks, None}}, 
    PlotLabel -> 
     StringJoin[Riffle[ConfigOfRun[run], "/"]] <> " [" <> fn <> "]"], 
   Plot[Evaluate[Table[curve[p], {p, ps}]], {x, 0, xMax}, 
    PlotStyle -> colours,PlotRange->All], 
   Epilog -> 
    Inset[Column@
      MapThread[
       Style, {StringForm["p = `1`", #] & /@ ps, 
        Take[colours, Length[ps]]}], Scaled[{0.9, 0.9}], {1, 1}]]];

extrapPlot[config_String, u_, adjusted_:False, opts1_:{}, opts2_:{}] :=
 MapThread[extrapolationPlot[RunsInConfiguration[config][[-1]], 
   #1, u, Join[#2, {Adjustment -> adjusted}]] &, {{Abs, Arg}, {opts1, opts2}}];

DefineMemoFunction[strain[run_, p_, adj_], 
 DataTableInterval[
  ReadRadiallyExtrapolatedStrain[run, 2, 2, strainFrequency[run], p, 
   RadialCoordinateTransformation -> RadialToTortoise, 
   PerturbativeAdjustment -> adj], {0, 1000}]]

plotPerturbativeAdjustment[config_] :=
Module[{h, p, adj, err, a, phi, run, caption, legend, f},
  run = RunsInConfiguration[config][[-1]];
  Do[h[p, adj] = strain[run, p, adj], {p, 0, 2}, {adj, {False, True}}];
  
  Do[{err[p, a, adj], 
     err[p, phi, adj]} = {0.01, 1} ampPhaseErrorsOfWaveforms[
      run, {h[p, adj], h[p + 1, adj]}], {p, 0, 
    1}, {adj, {False, True}}];
  
  caption[f_, adj_] := 
   Switch[f, a, "Amplitude", phi, "Phase"] <> " (" <> 
    If[adj, "adjusted", "standard"] <> ")";
  
  legend[a, p_] := (Subscript["A", p] - Subscript["A", p + 1])/
    Subscript["A", p + 1];
  legend[phi, 
    p_] := (Subscript["phi", p] - Subscript["phi", p + 1]);
  Switch[f, a, {0, 0.1}, phi, {0, 0.5}];
  GraphicsGrid[Table[
    PresentationListLinePlot[
     Table[Abs@err[p, f, adj], {p, 0, 1}], 
     PlotLabel -> caption[f, adj], 
     PlotRange -> {{0, All}, {0, 
        Max @@ Flatten@
          Table[Max[
            Abs@DataTableInterval[err[p, f, adjp], {200, 900}]], {p, 
            0, 1}, {adjp, {False, True}}]}}, 
     PlotLegend -> Table[legend[f, p], {p, 0, 1}], 
     LegendPosition -> {Right, Top}], {adj, {False, 
      True}}, {f, {a, phi}}], ImageSize -> 500]];

End[];

EndPackage[];
